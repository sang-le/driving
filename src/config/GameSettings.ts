class GameSettings {
	/**
	 * Road, Segments Settings
	 */
	public roadWidth = 3000;
	public segmentLength = 2000;
	public segmentDivisionBy = 5;
	public drawDistance = 125;
	public shouldDrawBallSegments = 50;

	/**
	 * Player, Balls, Ledges Settings
	 */
	public ledgeWidth = 3000;
	public playerWidth = 500;
	public ballSize = 500;
	public ballMarginRumble = 350;
	public maxSpeed = this.segmentLength * 9;
	public startPlayerSpeed = 3000;
	public respawnTime = 3000;
	public respawnTwinklingTime = 500;

	/**
	 * Camera Settings
	 */
	public cameraHeight = 2700;
	public distanceCamToPlayer = 5000;
	public camTargetZPlus = 4000;
	public fieldOfView = 60;
	public cameraDepth = 1 / Math.tan( (this.fieldOfView / 2) * Math.PI / 180 );

	/**
	 * Other Settings: Gravity, Tail, Exit Door...
	 */
	public displayBalls = 30;
	public gravity = 110;
	public ledgeGravity = 700;
	public flyGravity = 200;
	public jumpGravity = 5000;
	public ledgeAlpha = 90;
	public jumpBounceAlpha = 25;
	public flyAlpha = 35;
	public flyBounceAlpha = 45;
	public amountBounce = 1;
	public spriteScale = 0;
	public lightPosition = 300;
	public tailLength = 3000;
	public tailPoints = 10;
	public exitDoorSize = 3200;

	/**
	 * Unused Settings, Should be removed
	 */
	public fogDensity = 5;
	public accel = this.maxSpeed / 50;
	public decel = -this.maxSpeed / 70;
	public screechDecel = -this.maxSpeed / 100;
	public breaking = -this.maxSpeed / 20;
	public offRoadDecel = -this.maxSpeed / 10;
	public offRoadLimit = this.maxSpeed / 4;
	public centrifugal = 0.2;
	public steerCompensation = 0.5;
	public turnResetMultiplier = 0.1;
	public cameraAngleResetMultiplier = 0.07;
	public isHasPavement = false;
}

export const gameSettings = new GameSettings();
