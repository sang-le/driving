import {Util} from '../Components/Util';
import {getScreenHeight, getScreenWidth} from '../utils/viewport';

// phaser game config
const resolution = Util.getDevicePixelRatio();
const gameWidth = getScreenWidth();
const gameHeight = getScreenHeight();

export const gameConfig: GameConfig = {
	type: Phaser.AUTO,
	scale: {
		parent: 'game-container',
		mode: Phaser.Scale.NONE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		width: gameWidth * resolution,
		height: gameHeight * resolution,
		zoom: 1 / resolution,
	},
	resolution: 1,
	render: {
		pixelArt: false,
	},
};
