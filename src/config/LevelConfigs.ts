import {BALL_BLUE, BALL_GRAY, BALL_GREEN, BALL_ORANGE, BALL_PINK, BALL_RED, BALL_YELLOW} from '../constants/BallColor';

const configs = [{
	player: {
		color: BALL_BLUE,
	},
	road: [
	// 	{
	// 	time: 1,
	// 	isLedge: true,
	// 	frame: 'ledge',
	// },
	// 	{
	// 	time: 1.6,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
		{
		time: 1.6,
		offset: 'RANDOM',
		frame: 'ball',
		colors: ['PLAYER_COLOR'],
		autoMove: true,
		amount: 1,
	}, {
		time: 2.23,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 2.90,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 3.5,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}
	, {
		time: 4.2,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 4.9,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 5.56,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 6.25,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 6.9,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 7.5,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 8.2,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 8.9,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 9.55,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 10.2,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	},
	{
		time: 10.9,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 11.2,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 12.3,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 13.2,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 13.7,
		offset: [-0.65, 0, 0.65],
		frame: 'ball',
		colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
		amount: 3,
	}, {
		time: 15,
		isLedge: true,
		frame: 'ledge',
	},
	// {
	// 	time: 32,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
	// {
	// 	time: 33.5,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
	// {
	// 	time: 34.8,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
	// {
	// 	time: 36,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
	// {
	// 	time: 39,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_YELLOW, BALL_GRAY, 'PLAYER_COLOR'],
	// 	amount: 3,
	// },
	// {
	// 	time: 8,
	// 	isLedge: true,
	// 	frame: 'ledge',
	// 	color: BALL_RED,
	// }, {
	// 	time: 12,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_ORANGE, 'PLAYER_COLOR', BALL_YELLOW],
	// 	amount: 3,
	// }, {
	// 	time: 16,
	// 	offset: [-0.65, 0],
	// 	frame: 'ball',
	// 	colors: [BALL_RED, BALL_PINK],
	// 	amount: 2,
	// }, {
	// 	time: 20,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: ['PLAYER_COLOR', BALL_BLUE, BALL_PINK],
	// 	amount: 3,
	// }, {
	// 	time: 24,
	// 	offset: 'DEFAULT',
	// 	frame: 'ball',
	// 	colors: [BALL_BLUE, BALL_PINK, 'PLAYER_COLOR'],
	// 	amount: 1,
	// }, {
	// 	time: 28,
	// 	offset: [0],
	// 	frame: 'ball',
	// 	colors: [BALL_ORANGE, 'PLAYER_COLOR', BALL_YELLOW],
	// 	amount: 1,
	// }, {
	// 	time: 32,
	// 	offset: 'RANDOM',
	// 	frame: 'ball',
	// 	colors: ['PLAYER_COLOR', BALL_PINK, BALL_GREEN],
	// 	autoMove: true,
	// 	amount: 1,
	// }, {
	// 	time: 36,
	// 	offset: [-0.65, 0, 0.65],
	// 	frame: 'ball',
	// 	colors: [BALL_GREEN, 'PLAYER_COLOR', BALL_ORANGE],
	// 	autoMove: false,
	// 	amount: 3,
	// }, {
	// 	time: 40,
	// 	isLedge: true,
	// 	frame: 'ledge',
	// }
	],
	roadRandom: {
		lastTime: 17,
		distanceBall: 2,
		distanceLedge: 8,
	},
}];

export default configs;
