export const isInViewport = elem => {
  if (!elem) return false;
  const bounding = elem.getBoundingClientRect();
  return (
    bounding.top >= 0 &&
    bounding.left >= 0 &&
    bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

export const isVisibleInViewport = elem => {
  if (!elem) return false;
  const bounding = elem.getBoundingClientRect();
  const viewportTop = 0;
  const viewportBottom =
    viewportTop + (window.innerHeight || document.documentElement.clientHeight);
  return bounding.bottom > viewportTop && bounding.top < viewportBottom;
};

export const getScreenWidth = (percent = 100) => {
  const maxWidth =
    window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0;
  return (maxWidth / 100) * percent;
};

export const getScreenHeight = () => {
  return (
    window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0
  );
};

export const getMinScreenSize = () => {
  const width = getScreenWidth();
  const height = getScreenHeight();

  return width > height ? height : width;
};

export const supportViewportUnits = () => {
  document.body.addEventListener('touchend', () => {
    document.body.scrollTop = 0;
  });
};
