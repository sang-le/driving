import {Ball} from '../Components/Ball';
import {BallShadow} from '../Components/BallShadow';

export interface IBallProps {
	key: string;
	col: number;
	trackPosition: number;
	offset: number;
	color: string;
	velocity: number;
	moveStatus: string;
	autoMove: boolean;
	percent: number;
	ball: Ball;
	shadow: BallShadow;
}
