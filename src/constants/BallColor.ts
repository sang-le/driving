export const BALL_YELLOW = 0xebac13;
export const BALL_BLUE = 0x02a1f1;
export const BALL_PURPLE = 0x7d04f9;

export const STRING_COLORS = [
	'BALL_YELLOW',
	'BALL_PURPLE',
	'BALL_BLUE',
];

export const COLORS = {
	BALL_YELLOW,
	BALL_BLUE,
	BALL_PURPLE,
};

export default [
	BALL_YELLOW,
	BALL_BLUE,
	BALL_PURPLE,
];
