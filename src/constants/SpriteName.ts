export const PLAYER = 'ball.png';
export const BALL = 'ball.png';
export const ROAD = 'road.png';
export const TAIL = 'tail.png';
export const GUILD_LINE = 'guild-line.png';
export const LEDGE = 'ledge.png';
export const HAND = 'hand.png';
export const BALL_SHADOW = 'ball-shadow.png';
export const ARROW_LEFT = 'arrow-left.png';
export const ARROW_RIGHT = 'arrow-right.png';
export const BLUE_DOT = 'blue-dot.png';
export const EXIT_DOOR = 'exit-door.png';
export const FLARE_DIE_BOT = 'flare-die-bot.png';
export const FLARE_DIE_TOP = 'flare-die-top.png';
export const TIME_LINE_BLUE = 'timeline-blue.png';
export const TIME_LINE_PINK = 'timeline-pink.png';
export const TIME_LINE_STAR = 'timeline-star.png';
export const ARROW_GUIDE = 'arrow-guide.png';
export const BALL_PURPLE = 'ball-purple.png';
export const BALL_YELLOW = 'ball-yellow.png';
export const BALL_BLUE = 'ball-blue.png';
export const BALL_FRAMES = {
	BALL_PURPLE,
	BALL_YELLOW,
	BALL_BLUE,
};
