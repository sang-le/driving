import './libs/GLTFLoader';
import 'phaser';
import '@csstools/normalize.css';
import './css/styles.css';
import { BootScene } from './scenes/BootScene';
import { gameConfig } from './config/GameConfig';
import { LoadScene } from './scenes/LoadScene';
import { GameScene } from './scenes/GameScene';
import { RaceUiScene } from './scenes/RaceUiScene';
import { InfoUiScene } from './scenes/InfoUiScene';

// set up game class, and global stuff
export class GameApp extends Phaser.Game {
	private debug: boolean = false;

	constructor(config: GameConfig) {
		super(config);
	}

	destroy(removeCanvas: boolean, noReturn?: boolean) {
		super.destroy(removeCanvas, noReturn);
	}
}

// start the game
window.onload = () => {
	const game = new GameApp(gameConfig);

	game.scene.add('BootScene', BootScene, true);
	game.scene.add('LoadScene', LoadScene, false);
	game.scene.add('GameScene', GameScene, false);
	game.scene.add('RaceUiScene', RaceUiScene, false);
	game.scene.add('InfoUiScene', InfoUiScene, false);

	window.game = game;
};

window.onbeforeunload = () => {
	window.game.destroy();
}
