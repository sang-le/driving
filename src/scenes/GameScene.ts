import {BaseScene} from './BaseScene';
import {Road} from '../Components/Road';
import {TourGuide} from '../Components/TourGuide';
import Player from '../Components/Player';
import {MusicManager} from '../Components/MusicManager';
import {PLAYER} from '../constants/SpriteName';
import {ExitDoor} from '../Components/ExitDoor';
import {MiniRoad} from '../Components/MiniRoad';
import {WindManager} from '../Components/WindManager';
import {CameraManager} from '../Components/CameraManager';
import {Sky} from '../Components/Sky';
import {SkyManager} from '../Components/SkyManager';
import {BallManager} from '../Components/BallManager';
import {LedgeManager} from '../Components/LedgeManager';
import {Score} from '../Components/Score';
import {PointerController} from '../Components/PointerController';

export class GameScene extends BaseScene {
	public position: number;
	public player: Player;
	public road: Road;
	public debugText: Phaser.GameObjects.BitmapText;
	public sky: Phaser.GameObjects.Image;
	public camera: Phaser.Cameras.Scene2D.Camera;
	public guild: TourGuide;
	public musicManager: MusicManager;
	public isStarted: boolean;
	public levelConfig: any;
	public musicConfigs: any;
	public exitDoor: ExitDoor;
	public miniRoad: MiniRoad;
	public wind: WindManager;
	public cameraManager: CameraManager;
	public ballManager: BallManager;
	public ledgeManager: LedgeManager;
	public score: Score;
	public speedRate: number;
	public pointerController: PointerController;
	public skyManager: SkyManager;

	private startText: any;
	private continueText: any;
	private cancelText: any;

	constructor(key: string, options: any) {
		super('GameScene');

		this.speedRate = 1;
	}

	public create(): void {
		this.scene.launch('InfoUiScene', this);

		this.musicConfigs = this.cache.json.get('music-config');

		const gameWidth = this.scale.gameSize.width;
		const gameHeight = this.scale.gameSize.height;
		this.isStarted = false;
		this.camera = this.cameras.main;

		this.skyManager = new SkyManager(this);
		// this.sky = new Sky(this, 0, 0, 'bg6', null);
		// this.sky.setOrigin(0.5, 0.5);
		// this.add.existing(this.sky);

		this.road = new Road(this);
		this.musicManager = new MusicManager(this);
		this.exitDoor = new ExitDoor(this);
		this.miniRoad = new MiniRoad(this);
		this.cameraManager = new CameraManager(this);
		this.player = new Player(this, PLAYER);
		this.guild = new TourGuide(this, gameWidth / 2, gameHeight - 50);
		this.wind = new WindManager(this, 60000, 'wind');
		this.ballManager = new BallManager(this);
		this.ledgeManager = new LedgeManager(this);
		this.score = new Score(this, gameWidth / 2, 100);
		this.pointerController = new PointerController(this);

		this.road.init();

		this.player.setFrame(this.musicConfigs.player.color);

		this.registry.set('score', 0);

		this.ledgeManager.init();

		this.ballManager.init();

		this.exitDoor.init();

		this.miniRoad.init();

		this.cameraManager.setTarget(this.player);
	}

	public update(time: number, delta: number): void {
		// Update track position
		this.player.updateTrackPosition(time, delta);

		// move left / right by mousedown
		this.player.move(time, delta);

		// Update player
		this.player.update(time, delta);

		// Update camera manager
		this.cameraManager.update(time, delta);

		this.player.drawPlayer(time, delta);

		// Update road
		this.road.update(time, delta);

		// Update exit door
		this.exitDoor.hide();

		this.exitDoor.update(time, delta);

		this.ballManager.update(time, delta);

		this.ledgeManager.update(time, delta);

		// Update mini road
		this.miniRoad.draw();

		// Update wind
		this.wind.update(time, delta);

		// update sky (not Sky Link)
		this.skyManager.update(time, delta);
	}

	public reset() {
		this.player.reset();

		this.score.reset();

		this.musicManager.reset();

		this.road.reset();

		this.ledgeManager.reset();

		this.ballManager.reset();

		this.pointerController.show();
	}

	public gameOver() {
		if (!this.continueText) {
			this.continueText = this.add.text(this.scale.gameSize.width / 2 - 40, this.scale.gameSize.height / 2 - 40, 'Continue', {
				fontSize: '30px',
			});

			this.continueText.setInteractive();

			this.continueText.on('pointerdown', () => {
				this.continue();
			}, this);
		} else {
			this.continueText.setVisible(true);
		}

		if (!this.cancelText) {
			this.cancelText = this.add.text(this.scale.gameSize.width / 2 - 40, this.scale.gameSize.height / 2 + 40, 'Cancel', {
				fontSize: '30px',
			});

			this.cancelText.setInteractive();

			this.cancelText.on('pointerdown', () => {
				console.log('Click cancel');
				this.cancel();
			}, this);
		} else {
			this.cancelText.setVisible(true);
		}
	}

	public restart() {
		this.scene.restart();
	}

	public continue() {
		this.player.continue();
		this.continueText.setVisible(false);
		this.cancelText.setVisible(false);
		this.pointerController.show();
	}

	public cancel() {
		this.reset();
		this.continueText.setVisible(false);
		this.cancelText.setVisible(false);
		this.pointerController.show();
	}
}
