import { BaseScene } from './BaseScene';
import { GameScene } from './GameScene';
import {Util} from '../Components/Util';

export class InfoUiScene extends BaseScene {
	public fpsText: Phaser.GameObjects.BitmapText;
	public gameScene: GameScene;

	public timer: Phaser.Tweens.Tween;

	constructor(key: string, options: any) {
		super('InfoUiScene');
	}

	public create(gameScene: GameScene): void {
		this.gameScene = gameScene;

		this.fpsText = this.add.bitmapText(this.scale.gameSize.width / 2, 120 * Util.getDevicePixelRatio(), 'numbers', '0', 10 * Util.getDevicePixelRatio());
		this.fpsText.setOrigin(0.5, 0.5);
	}

	public update(): void {
		const { actualFps } = this.game.loop;
		const fps = +actualFps.toFixed(2);
		this.fpsText.setText('fps: ' + fps.toString());
	}

}
