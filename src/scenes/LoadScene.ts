import { BaseScene } from './BaseScene';
import { Util } from '../Components/Util';
import {TEXTURE} from '../constants/CommonType';
import {ROAD} from '../constants/SpriteName';

export class LoadScene extends BaseScene {
	constructor(key: string, options: any) {
		super('LoadScene');
	}

	public preload(): void {
		const progress = this.add.graphics();

		this.load.on('progress', (value: number) => {
			progress.clear();
			progress.fillStyle(0xffffff, 1);
			progress.fillRect(
				0,
				this.scale.gameSize.height / 2,
				this.scale.gameSize.width * value,
				60,
			);
		});

		this.load.on('complete', () => {
			progress.destroy();
		});

		this.loadTexture();

		this.loadAtlas();

		this.loadAudio();

		this.loadFont();

		this.loadFileJson();
	}

	public create(): void {
		this.scene.start('GameScene', {});
	}

	private loadTexture() {
		const devicePixelRatio = Util.getDevicePixelRatio();

		this.load.spritesheet(
			'effect-collision-same-color',
			'./assets/effect/effect-collision.png',
			{ frameWidth: 128, frameHeight: 128 }
		);
		this.load.spritesheet(
			'effect-jump-down-collision-road',
			'./assets/effect/effect-jump-down-collision-road.png',
			{ frameWidth: 40, frameHeight: 40 }
		);

		this.load.image('bg6', `./assets/bg/${devicePixelRatio}x/6.jpg`);
		this.load.image('bgtop', `./assets/bg/${devicePixelRatio}x/skytop.jpg`);
	}

	private loadAtlas() {
		const devicePixelRatio = Util.getDevicePixelRatio();

		const image = `./assets/${devicePixelRatio}x/${TEXTURE}.png`;
		const json = `./assets/${devicePixelRatio}x/${TEXTURE}.json`;

		this.load.atlas(TEXTURE, image, json);

		this.load.image('segment', './assets/road-256.png');
		this.load.image('wind', './assets/wind.png');
	}

	private loadAudio() {
		this.load.audio('music', ['./assets/music/faded.mp3']);
	}

	private loadFont() {
		// this.load.bitmapFont('numbers', './assets/fonts/number-font.png', './assets/fonts/number-font.xml');
		this.load.bitmapFont('impact', './assets/fonts/impact-24-outline.png', './assets/fonts/impact-24-outline.xml');
		this.load.bitmapFont('numbers', './assets/fonts/arial.png', './assets/fonts/arial.fnt');
	}

	private loadFileJson() {
		this.load.json('music-config', 'assets/music/faded.json');
	}
}
