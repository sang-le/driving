import { BaseScene } from './BaseScene';
import { GameScene } from './GameScene';

export class RaceUiScene extends BaseScene {
	public timerText: Phaser.GameObjects.BitmapText;
	public timeLargeText: Phaser.GameObjects.BitmapText;
	public timeSmallText: Phaser.GameObjects.BitmapText;
	public gameScene: GameScene;
	public timer: Phaser.Tweens.Tween;

	constructor(key: string, options: any) {
		super('RaceUiScene');
	}

	public create(gameScene: GameScene): void {
		this.gameScene = gameScene;
		this.timeLargeText = this.add.bitmapText(this.scale.gameSize.width / 2 + 30, 5, 'numbers', '000', 32).setOrigin(1, 0).setTint(0xffcccc);
		this.timeSmallText = this.add.bitmapText(this.scale.gameSize.width / 2 + 33, 8, 'numbers', '000', 16).setOrigin(0, 0).setTint(0xffcccc);
		this.timerText = this.add.bitmapText(this.scale.gameSize.width / 2 + 54, 8, 'impact', 'time', 16).setOrigin(0, 0);

		this.timer = this.tweens.addCounter({
			from: 180,
			to: 0,
			duration: 180000,
		});
	}

	public update(): void {
		const timerValue = this.timer.getValue().toFixed(2).split('.');
		this.timeLargeText.setText(timerValue[0]);
		this.timeSmallText.setText(timerValue[1]);
	}

	public destroy(): void {
		this.registry.events.off('changedata');
	}
}
