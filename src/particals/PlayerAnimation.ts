import { PlayerEffects } from '../effects/PlayerEffects';
import {TEXTURE} from '../constants/CommonType';
import {FLARE_DIE_BOT, FLARE_DIE_TOP} from '../constants/SpriteName';

export class PlayerAnimation {
	public particles: any;
	public scene: any;
	public target: any;
	public moveAnimation: any;
	public parCollisionSameColor: any;
	public emitterCollisionSameColor: any;
	public parCollisionOtherColor: any;
	public emitterCollisionOtherColor: any;
	public flare: any;
	public tweenFlare: any;
	public flareCircle: any;
	public tweenFlareCircleShow: any;
	public tweenFlareCircleHide: any;
	public score: any;
	public tweenScore: any;
	public effect: any;
	public parent: any;
	public tweenFlareDieTop: any;
	public tweenFlareDieBot: any;

	constructor(scene: any, parent: any, target: any) {
		this.scene = scene;
		this.target = target;
		this.parent = parent;
		this.effect = new PlayerEffects(scene, parent, target);
	}

	public collisionSameColor() {
		this.effect.playEffectCollisionSameColor();
	}

	public jumpDownCollisionRoad() {
		this.effect.playEffectJumpDownCollisionRoad();
	}

	public collisionOtherColor(key: string, onComplete?: any) {
		if (!this.parCollisionOtherColor || !this.emitterCollisionOtherColor) {
			this.parCollisionOtherColor = this.scene.add.particles(TEXTURE);
			this.emitterCollisionOtherColor = this.parCollisionOtherColor.createEmitter({
				x: this.target.x,
				y: this.target.y,
				speed: { min: 100, max: 150 },
				angle: { min: 0, max: 360 },
				alpha: { min: 0.2, max: 0.8 },
				scale: { start: 0.1, end: 0.5 },
				blendMode: 'SCREEN',
				lifespan: { min: 200, max: 600 },
				quantity: 80,
				maxParticles: 80,
				// emitCallback: onComplete,
				delay: 100,
				frame: [key],
			});
			this.parCollisionOtherColor.setDepth(this.target.depth);

			const flareDieTop = this.scene.add.image(this.target.x, this.target.y, TEXTURE, FLARE_DIE_TOP).setScale(0);
			flareDieTop.setDepth(this.target.depth);

			this.tweenFlareDieTop = this.scene.add.tween({
				targets: flareDieTop,
				alpha: 0,
				scaleY: '+=0.3',
				scaleX: '+=0.48',
				duration: 1000,
				delay: 0,
				y: '-=40',
				ease: 'Linear',
			});

			const flareDieBot = this.scene.add.image(this.target.x, this.target.y, TEXTURE, FLARE_DIE_BOT).setScale(0);
			flareDieBot.setDepth(this.target.depth);

			this.tweenFlareDieBot = this.scene.add.tween({
				targets: flareDieBot,
				alpha: 0,
				scaleY: '+=0.4',
				scaleX: '+=0.5',
				duration: 1000,
				delay: 0,
				y: '+=45',
				ease: 'Linear',
				onComplete,
			});
		}

		this.tweenFlareDieTop.restart();
		this.tweenFlareDieBot.restart();

		this.emitterCollisionOtherColor.dead = [];
		this.emitterCollisionOtherColor.start();
	}

	public update(time: number, delta: number) {
		if (this.flare) {
			this.flare.setPosition(this.target.x, this.target.y);
		}
		if (this.flareCircle) {
			this.flareCircle.setPosition(this.target.x, this.target.y);
		}
		if (this.score) {
			this.score.setPosition(this.target.x, this.target.y - this.target.displayHeight);
		}

		this.effect.update();
		// if (this.emitterCollisionSameColor) {
		// 	const alive = this.emitterCollisionSameColor.alive;
		//
		// 	if (alive && alive.length > 0) {
		// 		console.log(alive[0])
		// 		console.log(alive[0].x, alive[0].y, alive[0].velocityX, alive[0].velocityY)
		//
		// 		// alive.forEach((item: any) => {
		// 		// 	item.x = this.target.x;
		// 		// 	item.y = this.target.y;
		// 		// });
		// 	}
		// }
	}
}
