import {Util} from '../Components/Util';
import {COLORS} from '../constants/BallColor';
import {TEXTURE} from '../constants/CommonType';

export class PlayerEffects {
	public scene: any;
	public target: any;
	public parent: any;
	private spriteCollisionSameColor: any;
	private spriteJumpDownCollisionRoad: any;
	private color: any;

	constructor(scene: any, parent: any, target: any) {
		this.scene = scene;
		this.target = target;
		this.parent = parent;

		this.init();
	}

	public init() {
		this.addEffectCollisionSameColor();
		this.addEffectJumpDownCollisionRoad();
	}

	public playEffectCollisionSameColor() {
		if (!this.spriteCollisionSameColor.visible) {
			this.spriteCollisionSameColor.setVisible(true);
		}

		if (this.parent.color !== this.color) {
			// @ts-ignore
			this.spriteCollisionSameColor.setTint(COLORS[this.parent.color]);
			this.color = this.parent.color;
		}
		this.spriteCollisionSameColor.anims.play('collision-same-color');
	}

	public playEffectJumpDownCollisionRoad() {
		if (!this.spriteJumpDownCollisionRoad.visible) {
			this.spriteJumpDownCollisionRoad.setVisible(true);
		}

		this.spriteJumpDownCollisionRoad.anims.play('jump-down-collision-road');
	}

	public update() {
		if (this.spriteCollisionSameColor) {
			this.spriteCollisionSameColor.setPosition(this.target.x, this.target.y);
		}

		if (this.spriteJumpDownCollisionRoad) {
			this.spriteJumpDownCollisionRoad.setPosition(this.target.x, this.target.y + this.target.displayHeight / 2);
		}
	}

	private addEffectCollisionSameColor() {
		const key = 'collision-same-color';
		const sprite = 'effect-collision-same-color';
		const config = {
			key,
			frames: this.scene.anims.generateFrameNumbers(sprite),
			frameRate: 24,
			showOnStart: false,
			hideOnComplete: true,
		};
		this.scene.anims.create(config);
		this.spriteCollisionSameColor = this.scene.add.sprite(this.target.x, this.target.y, sprite);
		this.spriteCollisionSameColor.setScale(this.target.scale * 4);
		this.spriteCollisionSameColor.setDepth(this.target.depth + 1);
		this.spriteCollisionSameColor.setVisible(false);
		this.spriteCollisionSameColor.anims.load(key);
	}

	private addEffectJumpDownCollisionRoad() {
		const key = 'jump-down-collision-road';
		const sprite = 'effect-jump-down-collision-road';
		const config = {
			key,
			frames: this.scene.anims.generateFrameNumbers(sprite),
			frameRate: 24,
			showOnStart: false,
			hideOnComplete: true,
		};
		this.scene.anims.create(config);
		this.spriteJumpDownCollisionRoad = this.scene.add.sprite(this.target.x, this.target.y, sprite);
		this.spriteJumpDownCollisionRoad.setScale(this.target.scale * 1.5);
		this.spriteJumpDownCollisionRoad.setDepth(this.target.depth);
		this.spriteJumpDownCollisionRoad.setVisible(false);
		this.spriteJumpDownCollisionRoad.anims.load(key);
	}
}
