import {gameSettings} from '../../config/GameSettings';
import {Util} from '../Util';
import Player from '../Player';
import {GameScene} from '../../scenes/GameScene';

export default class TurnController {
	private player: Player;
	private isPointerDowning: boolean = false;
	private deltaPointerUpX: number = 0;
	private scene: GameScene;

	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
	}

	public update(time: number, delta: number) {
		if (!this.player.playerSegment) {
			return;
		}

		const pointer = this.scene.input.activePointer;

		// Touching
		if (pointer.isDown) {
			const deltaPlayerWorldX = ((this.player.image.x - pointer.worldX) / (this.scene.scale.gameSize.width / 2))
				* gameSettings.roadWidth;

			this.player.x = Util.ease(this.player.x, this.player.x + deltaPlayerWorldX, 0.2);

			if (!this.isPointerDowning) {
				this.isPointerDowning = true;
			}

			return;
		}

		// Handle running without touches
		const playerWorldX = Util.interpolate(
			this.player.playerSegment.p1.world.x,
			this.player.playerSegment.p2.world.x, this.player.playerPercent);

		const min = playerWorldX - gameSettings.roadWidth / 2 + gameSettings.playerWidth / 2;
		const max = playerWorldX + gameSettings.roadWidth / 2 - gameSettings.playerWidth / 2;

		if (this.isPointerDowning) {
			this.deltaPointerUpX = this.player.x > playerWorldX ?
				Math.abs((this.player.x - playerWorldX) / (max - playerWorldX))
				: Math.abs((playerWorldX - this.player.x) / (playerWorldX - min));

			this.isPointerDowning = false;
		}

		const deltaPointerUpValueX = this.player.x > playerWorldX ?
			this.deltaPointerUpX * (max - playerWorldX) : this.deltaPointerUpX * (min - playerWorldX);

		this.player.x = playerWorldX + deltaPointerUpValueX;
	}
}
