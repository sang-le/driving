import Player from '../Player';
import {Util} from '../Util';
import {Ball} from '../Ball';
import {GameScene} from '../../scenes/GameScene';
import {BLUE_DOT} from '../../constants/SpriteName';

export default class CollisionBehavior {
	private scene: GameScene;
	private readonly player: Player;

	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
	}

	public collisionBall(ball: Ball, delta: number) {
		if (Util.overlapPlayer(this.player, ball, delta)) {
			// Collision same color
			if (this.player.frame === ball.getFrame()) {
				this.collisionSameColor(ball);
			} else { // GAME OVER
				this.collisionOtherColor(ball);
			}
		}
	}

	private collisionSameColor(ball: Ball) {
		this.player.anim.collisionSameColor();
		this.scene.ballManager.hideBall(ball);
		this.scene.score.addScore(10);
	}

	private collisionOtherColor(ball: Ball) {
		this.scene.ballManager.hideBall(ball);
		this.player.dying();
		this.player.anim.collisionOtherColor(BLUE_DOT, this.scene.gameOver.bind(this.scene));
	}
}
