import Player from '../Player';
import {GameScene} from '../../scenes/GameScene';
import {
	RESPAWN_COMPLETED, RESPAWNED,
	RESPAWNED_RUNNING,
	RESPAWNING, RUNNING,
} from '../../constants/PlayerStatus';
import {gameSettings} from '../../config/GameSettings';

export default class RespawnBehavior {
	private scene: GameScene;
	private player: Player;
	private timeRespawn: number;
	private alphaRate: number;
	private state: string;
	private respawningTime: number;

	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
		this.timeRespawn = 0;
		this.respawningTime = 300;
	}

	public getState() {
		return this.state;
	}

	public setTimeRespawn(timeRespawn: number) {
		this.timeRespawn = timeRespawn;
	}

	public update(time: number, delta: number) {
		if (this.state === RESPAWN_COMPLETED) { return; }

		if (this.state === RESPAWNING) {
			this.respawningTime -= delta;

			if (this.respawningTime <= 0) {
				this.state = RESPAWNED;
				this.player.status = RESPAWNED;
			}

			return;
		}

		if (this.timeRespawn <= 0) {
			this.startNewLife();
			return;
		}

		const deltaAlpha = 1 / gameSettings.respawnTwinklingTime * delta;

		if (this.player.image.alpha <= 0.1) {
			this.alphaRate = 1;
		}

		if (this.player.image.alpha >= 1) {
			this.alphaRate = -1;
		}

		this.player.image.alpha += this.alphaRate * deltaAlpha;

		this.timeRespawn -= delta;
	}

	public respawn() {
		this.player.playerSegment.reset();
		this.player.image.setVisible(true);
		this.player.image.setActive(true);
		this.player.image.setAlpha(1);
		this.player.speed = 0;
		this.state = RESPAWNING;
		this.player.status = RESPAWNING;
		this.respawningTime = 300;
	}

	public run() {
		this.state = RESPAWNED_RUNNING;
		this.setTimeRespawn(gameSettings.respawnTime);
		this.player.speed = gameSettings.startPlayerSpeed;
		this.player.shadow.setVisible(false);
		this.player.tail.start();
		this.player.status = RUNNING;
	}

	private startNewLife() {
		this.player.image.alpha = 1;
		this.state = RESPAWN_COMPLETED;
		this.timeRespawn = 0;
	}

}
