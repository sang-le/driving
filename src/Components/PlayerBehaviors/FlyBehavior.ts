
import Player from '../Player';
import {DOWN, FLYING, JUMPING, RUNNING, UP} from '../../constants/PlayerStatus';
import {gameSettings} from '../../config/GameSettings';
import {Util} from '../Util';
import {GameScene} from '../../scenes/GameScene';

export default class FlyBehavior {
	private scene: GameScene;
	private player: Player;
	private gravity: number = gameSettings.flyGravity;
	private alpha: number = 45;
	private startAt: number;
	private startPosition: any;
	private maxHeight: number;
	private flewHeight: number;
	private direction: string;
	private bounced: number = 0;

	/**
	 *
	 * @param scene
	 * @param player
	 */
	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
	}

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public update(time: number, delta: number) {
		if (this.direction === UP) {
			this.up(time, delta);
		} else {
			this.down(time, delta);
		}
	}

	/**
	 *
	 * @param time
	 * @param alpha
	 */
	public start(time: number, alpha: number) {
		if (this.player.status === FLYING || this.player.status === JUMPING) { return; }

		this.startPosition = {
			x: this.player.x,
			z: this.player.trackPosition,
			y: this.player.y,
		};

		this.startAt = time;
		this.alpha = alpha;
		this.flewHeight = 0;
		this.direction = UP;
		this.maxHeight = Util.getJumpMaxHeight(this.player.speed, this.alpha, this.gravity);

		this.player.status = FLYING;
	}

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public up(time: number, delta: number) {
		const deltaTimeFly = (time - this.startAt) * 0.01;
		const currentHeight = this.flewHeight;

		this.flewHeight = Util.getJumpY(this.player.speed, this.alpha, deltaTimeFly, this.gravity);
		this.direction = UP;

		this.player.status = FLYING;
		this.player.y = this.startPosition.y + this.flewHeight;

		if (this.flewHeight < currentHeight) {
			this.direction = DOWN;
		}
	};

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public down(time: number, delta: number) {
		const deltaTimeJump = (time - this.startAt) * 0.01;
		const segment = this.scene.road.findSegmentByZ(this.player.trackPosition + gameSettings.distanceCamToPlayer);
		const playerPercent = Util.percentRemaining(this.player.trackPosition + gameSettings.distanceCamToPlayer, gameSettings.segmentLength);
		const currentY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, playerPercent) + gameSettings.playerWidth / 2;

		this.flewHeight = Util.getJumpY(this.player.speed, this.alpha, deltaTimeJump, this.gravity);
		this.player.y = this.startPosition.y + this.flewHeight;
		this.direction = DOWN;
		this.player.status = FLYING;

		if (this.player.y <= currentY) {
			this.stop();

			this.player.y = currentY;
			this.player.anim.jumpDownCollisionRoad();
			this.player.bounce(time, gameSettings.flyBounceAlpha);
		}
	}

	public stop() {
		this.player.status = RUNNING;
		this.direction = null;
	}

	public getHeight() {
		return this.flewHeight;
	}

	public getMaxHeight() {
		return this.maxHeight;
	}

	public getDirection() {
		return this.direction;
	}
}
