import Player from '../Player';
import {DOWN, FLYING, JUMPING, RUNNING, UP} from '../../constants/PlayerStatus';
import {gameSettings} from '../../config/GameSettings';
import {Util} from '../Util';
import {GameScene} from '../../scenes/GameScene';

export default class JumpBehavior {
	private scene: GameScene;
	private player: Player;
	private gravity: number = gameSettings.jumpGravity;
	private alpha: number = 45;
	private startAt: number;
	private startPosition: any;
	private maxHeight: number;
	private direction: string = UP;
	private jumpedHeight: number = 0;
	private bounced: number = 0;

	/**
	 *
	 * @param scene
	 * @param player
	 */
	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
	}

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public update(time: number, delta: number) {
		if (this.direction === UP) {
			this.up(time, delta);
		} else {
			this.down(time, delta);
		}
	}

	/**
	 *
	 * @param time
	 * @param alpha
	 * @param bounced
	 */
	public start(time: number, alpha: number, bounced: number = 0) {
		if (this.player.status === JUMPING || this.player.status === FLYING) { return; }

		this.startPosition = {
			x: this.player.x,
			z: this.player.trackPosition,
			y: this.player.y,
		};
		this.startAt = time;
		this.alpha = alpha;
		this.bounced = bounced;
		this.jumpedHeight = 0;
		this.direction = UP;
		this.maxHeight = Util.getJumpMaxHeight(this.player.speed, this.alpha, this.gravity);

		this.player.status = JUMPING;
	}

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public up(time: number, delta: number) {
		const deltaTimeJump = (time - this.startAt) * 0.01;
		const currentJumpedHeight = this.jumpedHeight;

		this.jumpedHeight = Util.getJumpY(this.player.speed, this.alpha, deltaTimeJump, this.gravity);
		this.player.y = this.startPosition.y + this.jumpedHeight;
		this.player.status = JUMPING;
		this.direction = UP;

		if (this.jumpedHeight < currentJumpedHeight) {
			this.direction = DOWN;
		}
	}

	/**
	 *
	 * @param time
	 * @param delta
	 */
	public down(time: number, delta: number) {
		const deltaTimeJump = (time - this.startAt) * 0.01;
		const segment = this.scene.road.findSegmentByZ(this.player.trackPosition + gameSettings.distanceCamToPlayer);
		const percent = Util.percentRemaining(this.player.trackPosition + gameSettings.distanceCamToPlayer, gameSettings.segmentLength);
		const currentY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, percent) + gameSettings.playerWidth / 2;

		this.jumpedHeight = Util.getJumpY(this.player.speed, this.alpha, deltaTimeJump, this.gravity);
		this.direction = DOWN;

		this.player.y = this.startPosition.y + this.jumpedHeight;
		this.player.status = JUMPING;

		if (this.player.y <= currentY) {
			this.player.y = currentY;
			this.player.anim.jumpDownCollisionRoad();

			if (this.bounced >= gameSettings.amountBounce) {
				this.stop();
			} else {
				this.player.status = null;
				this.start(time, gameSettings.jumpBounceAlpha);
				this.bounced++;
			}
		}
	}

	public stop() {
		this.bounced = 0;
		this.direction = null;
		this.player.status = RUNNING;
	}

	public getHeight() {
		return this.jumpedHeight;
	}

	public getMaxHeight() {
		return this.maxHeight;
	}

	public getDirection() {
		return this.direction;
	}
}
