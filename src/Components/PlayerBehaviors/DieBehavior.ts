import Player from '../Player';
import {GameScene} from '../../scenes/GameScene';
import {DEATH} from '../../constants/PlayerStatus';
import {gameSettings} from '../../config/GameSettings';

export default class DieBehavior {
	private scene: GameScene;
	private player: Player;

	constructor(scene: GameScene, player: Player) {
		this.scene = scene;
		this.player = player;
	}

	public update(time: number, delta: number) {
		this.dying(delta);
	}

	private dying(delta: number) {
		const deltaAlpha = 1 / gameSettings.respawnTwinklingTime * delta;

		this.player.image.alpha -= deltaAlpha;

		if (this.player.image.alpha <= 0) {
			this.player.image.alpha = 0;
			this.player.status = DEATH;
		}
	}
}
