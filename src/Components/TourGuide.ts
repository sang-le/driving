import {TEXTURE} from '../constants/CommonType';
import {ARROW_LEFT, ARROW_RIGHT, GUILD_LINE, HAND} from '../constants/SpriteName';

export class TourGuide {
	public scene: any;
	public x: number;
	public y: number;

	private hand: any;
	private tweenHand: any;
	private textGuild: any;
	private line: any;
	private arrowLeft: any;
	private tweenArrowLeft: any;
	private arrowRight: any;
	private tweenArrowRight: any;

	constructor(scene: any, x: number, y: number) {
		this.scene = scene;
		this.x = x;
		this.y = y;

		this.init();
	}

	public init() {
		const gameWidth = this.scene.scale.gameSize.width;
		const gameHeight = this.scene.scale.gameSize.height;
		const depth = 100;

		// hand guild
		this.hand = this.scene.add.image(100, this.y, TEXTURE, HAND);
		this.hand.setDepth(depth + 2);
		this.hand.setScale(0.7);

		this.tweenHand = this.scene.add.tween({
			targets: this.hand,
			x: gameWidth - 100,
			duration: 2500,
			ease(t: number) {
				return -(Math.cos(Math.PI * t) - 1) / 2;
			},
			yoyo: true,
			delay: 0,
			repeat: -1,
		});

		// bg line guild
		this.line = this.scene.add.image(gameWidth / 2, this.y - this.hand.displayHeight / 2, TEXTURE, GUILD_LINE);
		this.line.setDepth(depth);

		// arrow Left Guild
		this.arrowLeft = this.scene.add.image(gameWidth / 2, this.y - this.hand.displayHeight / 2, TEXTURE, ARROW_LEFT);
		this.arrowLeft.setDepth(depth + 1);
		this.arrowLeft.x -= this.arrowLeft.displayWidth / 2;

		this.tweenArrowLeft = this.scene.add.tween({
			targets: this.arrowLeft,
			x: 0,
			duration: 800,
			ease(t: number) {
				return -(Math.cos(Math.PI * t) - 1) / 2;
			},
			delay: 0,
			repeat: -1,
			alpha: 0,
		});

		// arrow Right Guild
		this.arrowRight = this.scene.add.image(gameWidth / 2, this.y - this.hand.displayHeight / 2, TEXTURE, ARROW_RIGHT);
		this.arrowRight.setDepth(depth + 1);
		this.arrowRight.x += this.arrowLeft.displayWidth / 2;

		this.tweenArrowRight = this.scene.add.tween({
			targets: this.arrowRight,
			x: gameWidth,
			duration: 800,
			ease(t: number) {
				return -(Math.cos(Math.PI * t) - 1) / 2;
			},
			delay: 0,
			repeat: -1,
			alpha: 0,
		});

		// text guild
		this.textGuild = this.scene.add.text(gameWidth / 2, this.y + 30, 'Giữ & kéo để điều khiển', { fontFamily: 'Arial', fontSize: 20, color: '#00ff00' });
		this.textGuild.setDepth(depth);
		this.textGuild.setOrigin(0.5);
	}

	public hide() {
		this.hand.setVisible(false);
		this.arrowLeft.setVisible(false);
		this.arrowRight.setVisible(false);
		this.line.setVisible(false);
		this.textGuild.setVisible(false);

		this.tweenHand.remove();
		this.tweenArrowLeft.remove();
		this.tweenArrowRight.remove();
	}
}
