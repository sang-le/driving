// @ts-ignore
import { Swipe } from 'phaser3-rex-plugins/plugins/gestures.js';
import { GameScene } from '../scenes/GameScene';

class SwipeController {
	public obj: any;
	public scene: GameScene;
	public configs: any;
	private right: any;
	private left: any;
	private up: any;
	private down: any;

	constructor(scene: GameScene, configs?: any) {
		this.scene = scene;
		this.configs = configs || { enable: true };

		this.obj = new Swipe(scene, configs);
		this.obj.on('swipe', (e: any) => {
			if (e.right) {
				console.log('RIGHT: Hacer algo a la derecha');
				if (this.right && typeof this.right === 'function') {
					this.right();
				}
			} else if (e.left) {
				console.log('LEFT: Hacer algo a la izquierda');
				if (this.left && typeof this.left === 'function') {
					this.left();
				}
			} else if (e.up) {
				console.log('UP: Hacer algo a la arriba');
				if (this.up && typeof this.up === 'function') {
					this.up();
				}
			} else if (e.down) {
				console.log('DOWN: Hacer algo a la abajo');
				if (this.down && typeof this.down === 'function') {
					this.down();
				}
			}
		}, this);
	}

	public swipeRight(right: any) {
		this.right = right;
	}

	public swipeLeft(left: any) {
		this.left = left;
	}

	public swipeUp(up: any) {
		this.up = up;
	}

	public swipeDown(down: any) {
		this.down = down;
	}
}

export default SwipeController;
