import {gameSettings} from '../config/GameSettings';
import {BallManager} from './BallManager';
import {TrackSegment} from './TrackSegment';
import {Util} from './Util';
import {COLORS} from '../constants/BallColor';
import {ArrowGuide} from './ArrowGuide';
import {TEXTURE} from '../constants/CommonType';
import {ARROW_GUIDE, BALL_FRAMES} from '../constants/SpriteName';

export class Ball {
	public index: number;
	private position: Phaser.Math.Vector3;
	private ballDrawer: any;
	private ballShadow: any;
	private readonly scene: any;
	private isActive: boolean;
	private percent: number;
	private color: string;
	private frame: string;
	private mainFrame: string;
	private mainColor: string;
	private offset: number;
	private velocity: number;
	private moveStatus: string;
	private isAutoMove: boolean;
	private isGuide: boolean;
	private arrowGuide: any;
	private readonly width: number;
	private manager: BallManager;
	private segment: TrackSegment;

	constructor(scene: any, manager: BallManager) {
		this.scene = scene;
		this.width = gameSettings.ballSize;
		this.manager = manager;

		this.isGuide = false;
		this.isActive = false;
		this.isAutoMove = false;
		this.velocity = 0;
	}

	public setPosition(x: number, y: number, z: number) {
		this.position = new Phaser.Math.Vector3(x, y, z);
	}

	public getPosition() {
		return this.position;
	}

	public setIsGuide(isGuide: boolean) {
		this.isGuide = isGuide;

		if (isGuide) {
			this.addGuide();
		}
	}

	public setAutoMove(isAutoMove: boolean) {
		this.isAutoMove = isAutoMove;
		this.moveStatus = 'RIGHT';
	}

	public setFrame(frame: string) {
		this.frame = frame;

		// @ts-ignore
		this.mainFrame = BALL_FRAMES[frame];
	}

	public setOffset(offset: number) {
		this.offset = offset;
	}

	public setVelocity(velocity: number) {
		this.velocity = velocity;
	}

	public setPercent(percent: number) {
		this.percent = percent;
	}

	public getPercent() {
		return this.percent;
	}

	public setActive(active: boolean) {
		this.isActive = active;
	}

	public setBallDrawer(ballDrawer: any) {
		this.ballDrawer = ballDrawer;
	}

	public getFrame() {
		return this.frame;
	}

	public hide() {
		if (!this.ballDrawer) { return; }

		this.manager.ballObjectPools.killAndHide(this.ballDrawer);
		this.manager.ballShadowObjectPools.killAndHide(this.ballShadow);
		this.ballDrawer = null;

		if (!this.arrowGuide) { return; }
		this.arrowGuide.hide();
	}

	public getDrawer() {
		return this.ballDrawer;
	}

	public setSegment(segment: TrackSegment) {
		this.segment = segment;
	}

	public getSegment() {
		return this.segment;
	}

	public update(time: number, delta: number) {
		this.updateDrawer(delta);

		this.setShadow();

		this.updateShadow();

		this.updateArrowGuide();
	}

	public addGuide() {
		this.arrowGuide = new ArrowGuide(this.scene, 0, 0, TEXTURE, ARROW_GUIDE);
	}

	private validate() {
		if (!this.isActive || !this.ballDrawer) { return false; }

		return true;
	}

	private updateDrawer(delta: number) {
		if (!this.validate()) { return; }

		const {
			x, y, w,
		} = this.scene.cameraManager.projectObject({
			x: this.position.x,
			y: this.position.y,
			z: this.position.z,
			width: this.width,
		});

		this.ballDrawer.setDepth(this.scene.road.segments.length - this.segment.index - this.percent);
		this.ballDrawer.setFrame(this.mainFrame);
		this.ballDrawer.setActive(true);

		if ((this.segment.show && this.segment.visible) || (y - w / 2) <= this.scene.road.maxY) {
			this.ballDrawer.setVisible(true);
		} else {
			this.ballDrawer.setVisible(false);
		}

		this.ballDrawer.setPosition(x, y);
		this.ballDrawer.setDisplaySize(w, w);

		this.autoMove(delta);
	}

	private updateArrowGuide() {
		if (!this.validate()) { return; }

		if (!this.arrowGuide) {
			return;
		}

		this.arrowGuide.draw(this);
	}

	private setShadow() {
		if (!this.validate()) { return; }

		if (this.ballShadow) { return; }

		const shadow = this.manager.getBallShadowObjectPools();
		this.ballShadow = shadow;
	}

	private updateShadow() {
		if (!this.validate()) { return; }

		if (!this.ballShadow) { return; }

		const {x, y, displayWidth, displayHeight, visible, active, depth} = this.ballDrawer;

		this.ballShadow.setVisible(visible);
		this.ballShadow.setActive(active);
		this.ballShadow.setDepth(depth);
		// @ts-ignore
		this.ballShadow.setTint(COLORS[this.frame]);
		this.ballShadow.setPosition(x, y + displayHeight);
		this.ballShadow.setDisplaySize(displayWidth, displayHeight);
		this.ballShadow.setAlpha(0.8);
	}

	private autoMove(delta: number) {
		if (!this.isAutoMove) { return; }

		const dlt = delta * 0.01;
		const worldCenterX = Util.interpolate(this.segment.p1.world.x, this.segment.p2.world.x, this.percent);
		const min = worldCenterX + (-gameSettings.roadWidth / 2 + gameSettings.ballMarginRumble + gameSettings.ballSize / 2);
		const max = worldCenterX + (gameSettings.roadWidth / 2 - gameSettings.ballMarginRumble - gameSettings.ballSize / 2);

		if (this.position.x <= min) {
			this.moveStatus = 'RIGHT';
		}
		if (this.position.x >= max) {
			this.moveStatus = 'LEFT';
		}

		if (this.moveStatus === 'RIGHT') {
			this.position.x += dlt * this.velocity;
		}

		if (this.moveStatus === 'LEFT' ) {
			this.position.x -= dlt * this.velocity;
		}
	}
}
