import {gameSettings} from '../config/GameSettings';
import {TrackSegment} from './TrackSegment';

export default class Road3D extends Phaser.GameObjects.Image {

	public gl: any;
	public pipeline3d;
	public program;

	public segments = [];
	public segmentCount = 200;
	private baseSegment: TrackSegment;
;
	public speed = gameSettings.startPlayerSpeed;

	public texture3d;
	public vertices;
	public texcoords;

	private defaultTexcoords = [
		0, 0,
		1, 0,
		0, 1,
		0, 1,
		1, 0,
		1, 1,
	];

	constructor(scene, x, y, texture) {
		super(scene, x, y, texture);
	}

	public setPlayer(player) {
		this.player = player;
		return this;
	}

	public setSegments(segments) {
		this.segments = segments;
		return this;
	}

	public renderWebGL(renderer, src, interpolationPercentage, camera, parentMatrix) {
		const pipeline = renderer.currentPipeline;

		renderer.clearPipeline();

		this.draw();

		renderer.rebindPipeline(pipeline);
	}

	public update(time: number, delta: number) {
		this.baseSegment = this.scene.cameraManager.baseSegment;

		this.buildSegmentVertexes();
	}

	public buildSegmentVertexes() {

		this.vertices = [];
		this.texcoords = [];
		this.segmentCount = 0;

		const halfRoadWidth = gameSettings.roadWidth / 2;

		const start = this.baseSegment.index;
		const end = (this.baseSegment.index + gameSettings.drawDistance) < this.segments.length ?
			(this.baseSegment.index + gameSettings.drawDistance) : this.segments.length;

		for (let i = end - 1; i >= start; i--) {
			if (!this.segments[i].visible) {
				continue;
			}

			const {p1, p2} = this.segments[i];
			const segment = {
				tl: {x: p2.world.x - halfRoadWidth, y: p2.world.y, z: p2.world.z},
				tr: {x: p2.world.x + halfRoadWidth, y: p2.world.y, z: p2.world.z},
				bl: {x: p1.world.x - halfRoadWidth, y: p1.world.y, z: p1.world.z},
				br: {x: p1.world.x + halfRoadWidth, y: p1.world.y, z: p1.world.z},
			};

			const {tl, tr, bl, br} = segment;

			const segmentVertexes = [
				tl.x, tl.y, tl.z,
				tr.x, tr.y, tr.z,
				bl.x, bl.y, bl.z,

				bl.x, bl.y, bl.z,
				tr.x, tr.y, tr.z,
				br.x, br.y, br.z,
			];

			this.vertices = this.vertices.concat(segmentVertexes);

			this.texcoords = this.texcoords.concat(this.getTextcoordsBySegmentId(i));

			this.segmentCount++;
		}

		return this;
	}

	private getTextcoordsBySegmentId(segmentId: number) {
		const divisionBy = gameSettings.segmentDivisionBy;
		const height = 1 / divisionBy;
		const yCoordTop = (divisionBy - 1 - segmentId % divisionBy) * height;
		const yCoordBottom = yCoordTop + height;

		const uvs = [
			0, yCoordTop,
			1, yCoordTop,
			0, yCoordBottom,

			0, yCoordBottom,
			1, yCoordTop,
			1, yCoordBottom,
		];

		return uvs;
	}

	public create() {
		const gl = this.pipeline.gl;
		this.gl = gl;

		// setup GLSL program
		this.program = webglUtils.createProgramFromScripts(gl, ['vertex-shader-3d', 'fragment-shader-3d']);

		// Create a buffer for positions
		const positionBuffer = gl.createBuffer();
		// Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
		// gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		// Put the positions in the buffer
		// this.setGeometry(gl);

		// provide texture coordinates for the rectangle.
		const texcoordBuffer = gl.createBuffer();
		// gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
		// Set Texcoords.
		// this.setTexcoords(gl);

		this.pipeline3d = {positionBuffer, texcoordBuffer};

		// Create a texture.
		const texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texture);

		this.texture3d = texture;

		// Fill the texture with a 1x1 blue pixel.
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
			new Uint8Array([0, 0, 255, 255]));

		/*// Asynchronously load an image
		const image = this.texture.source[0].image;

		// Now that the image has loaded make copy it to the texture.
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);*/

		// No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	}

	// Draw the scene.
	public draw() {
		const gl = this.gl;

		// webglUtils.resizeCanvasToDisplaySize(gl.canvas);

		// look up where the vertex data needs to go.
		const positionLocation = gl.getAttribLocation(this.program, 'a_position');
		const texcoordLocation = gl.getAttribLocation(this.program, 'a_texcoord');

		// lookup uniforms
		const matrixLocation = gl.getUniformLocation(this.program, 'u_matrix');
		const textureLocation = gl.getUniformLocation(this.program, 'u_texture');

		// Clear the canvas AND the depth buffer.
		// gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		// Tell it to use our program (pair of shaders)
		gl.useProgram(this.program);

		// Turn on the position attribute
		gl.enableVertexAttribArray(positionLocation);

		// Bind the position buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.pipeline3d.positionBuffer);
		this.setGeometry(gl);

		const image = this.texture.source[0].image;

		// Now that the image has loaded make copy it to the texture.
		gl.bindTexture(gl.TEXTURE_2D, this.texture3d);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

		// Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
		let size = 3;          // 3 components per iteration
		let type = gl.FLOAT;   // the data is 32bit floats
		let normalize = false; // don't normalize the data
		let stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		let offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(positionLocation, size, type, normalize, stride, offset);

		// Turn on the texcoord attribute
		gl.enableVertexAttribArray(texcoordLocation);

		// bind the texcoord buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.pipeline3d.texcoordBuffer);
		this.setTexcoords(gl);

		// Tell the texcoord attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
		size = 2;          // 2 components per iteration
		type = gl.FLOAT;   // the data is 32bit floats
		normalize = false; // don't normalize the data
		stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(texcoordLocation, size, type, normalize, stride, offset);

		// Set the matrix.
		gl.uniformMatrix4fv(matrixLocation, false, this.scene.cameraManager.viewProjectionMatrix);

		// Tell the shader to use texture unit 0 for u_texture
		gl.uniform1i(textureLocation, 0);

		// Draw the geometry.
		gl.drawArrays(gl.TRIANGLES, 0, this.segmentCount * 6);
	}

	// Fill the buffer with the values that define a letter 'F'.
	public setGeometry(gl) {
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertices), gl.STATIC_DRAW);
	}

	// Fill the buffer with texture coordinates the F.
	public setTexcoords(gl) {
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.texcoords), gl.STATIC_DRAW);
	}
}
