import {Sky} from './Sky';
import {GameScene} from '../scenes/GameScene';

export class SkyManager {
	private readonly scene: GameScene;
	private skies: Set<Sky> = new Set<Sky>();
	private position3d: Phaser.Math.Vector3;
	private skyTop: Sky;
	private skyCenter: Sky;
	private skyBot: Sky;
	private readonly gameWidth: number;
	private readonly gameHeight: number;
	private screen: any;
	private deltaCenterScreen: any;
	private center: any;

	constructor(scene: any) {
		const z = 10000000;
		this.gameWidth = scene.scale.gameSize.width;
		this.gameHeight = scene.scale.gameSize.height;

		this.scene = scene;
		this.position3d = new Phaser.Math.Vector3(0, 0, z);
		this.init();

		this.screen = null;
		this.deltaCenterScreen = null;
	}

	public init() {
		this.skyTop = new Sky(this.scene, 0, 0, 'bgtop', null);
		this.skyCenter = new Sky(this.scene, 0, 0, 'bg6', null);
		this.skyBot = new Sky(this.scene, 0, 0, 'bg6', null);

		this.skyTop.setPosition(this.gameWidth / 2, -this.skyTop.height / 2);
		this.skyCenter.setPosition(this.gameWidth / 2, this.gameHeight / 2);
		this.skyBot.setPosition(this.gameWidth / 2, this.gameHeight / 2 + this.skyCenter.height / 2 + this.skyBot.height / 2);

		this.skies.add(this.skyTop);
		this.skies.add(this.skyCenter);
		this.skies.add(this.skyBot);

		this.center = {
			x: this.skyCenter.width / 2,
			y: this.skyCenter.height / 2,
		};
	}

	public update(time: number, delta: number) {
		this.crop();
	}

	private projectNewPointScreen() {
		const {
			x, y, w,
		} = this.scene.cameraManager.projectObject({
			x: this.position3d.x,
			y: this.position3d.y,
			z: this.position3d.z,
			width: 10000,
		});

		if (!this.deltaCenterScreen) {
			this.deltaCenterScreen = {x: this.gameWidth / 2 - x, y: this.gameHeight / 2 - 320};
		}

		return {x: x + this.deltaCenterScreen.x, y: y + this.deltaCenterScreen.y};
	}

	private crop() {
		const newScreen = this.projectNewPointScreen();

		const topLeft = {
			x: this.center.x - newScreen.x,
			y: this.center.y - newScreen.y,
		};

		this.skyCenter.setCrop(
			topLeft.x,
			topLeft.y,
			this.gameWidth,
			this.gameHeight,
		);

		if (topLeft.y < 0) {
			this.skyTop.setVisible(true);
			this.skyTop.setCrop(
				topLeft.x,
				this.skyTop.height + topLeft.y,
				this.gameWidth,
				-topLeft.y,
			);

			this.skyTop.setPosition(newScreen.x, newScreen.y - this.skyTop.height / 2 - this.skyCenter.height / 2);
		} else {
			this.skyTop.setVisible(false);
		}

		if (this.gameHeight > (this.skyCenter.height  - topLeft.y)) {
			this.skyBot.setVisible(true);
			this.skyBot.setCrop(
				topLeft.x,
				0,
				this.gameWidth,
				this.gameHeight - (this.skyCenter.height  - topLeft.y),
			);

			this.skyBot.setPosition(newScreen.x, newScreen.y + this.skyCenter.height / 2 + this.skyBot.height / 2 );
		} else {
			this.skyBot.setVisible(false);
		}

		this.skyCenter.setPosition(newScreen.x, newScreen.y);
	}
}
