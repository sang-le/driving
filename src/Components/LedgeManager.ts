import {GameScene} from '../scenes/GameScene';
import {Util} from './Util';
import {gameSettings} from '../config/GameSettings';
import {Ledge} from './Ledge';
import {LEDGE} from '../constants/SpriteName';

export class LedgeManager {
	public ledges: Set<Ledge> = new Set<Ledge>();
	public lastLedge: Ledge;

	private readonly scene: GameScene;

	constructor(scene: GameScene) {
		this.scene = scene;
	}

	public init() {
		const { road: roadConfig, roadRandom: roadRandomConfig } = this.scene.musicConfigs;
		const ledgeConfigs = roadConfig.filter((config: any) => {
			return config.isLedge;
		});

		// tslint:disable-next-line:prefer-for-of
		for (let n = 0; n < ledgeConfigs.length; n++) {
			this.initLedge(ledgeConfigs[n]);
		}

		// tslint:disable-next-line:prefer-for-of
		for (let n = 0; n < roadRandomConfig.length; n++) {
			this.randomRender(roadRandomConfig[n]);
		}
	}

	public reset() {
		this.hideAll();
		this.ledges.clear();
		this.lastLedge = null;

		this.init();
	}

	public hideAll(): void {
		this.ledges.forEach( (ledge: Ledge) => {
			ledge.hide();
			const position = ledge.getPosition();
			const segment = ledge.getSegment();

			if (position.z < this.scene.player.trackPosition) {
				// Remove ball in list
				this.ledges.delete(ledge);

				// Remove ball on road
				if (segment.ledge) {
					segment.ledge = null;
				}
			}
		});
	}

	public update(time: number, delta: number) {
		this.hideAll();

		this.updateLedges(time, delta);
	}

	public getLastLedge(segmentLength: number): Ledge {
		let lastLedge = null;

		this.ledges.forEach( (ledge: Ledge) => {
			const segment = ledge.getSegment();
			if (segment.p2.world.z < segmentLength) {
				lastLedge = ledge;
			}
		});

		return lastLedge;
	}

	private updateLedges(time: number, delta: number) {
		this.ledges.forEach( (ledge: Ledge) => {
			ledge.update(time, delta);
		});
	}

	private randomRender(config: any) {
		const { distanceLedge, endTime, startTime } = config;
		const dlt = 1000 * 0.01;
		const playerSpeed = Util.getPlayerSpeed(this.scene.player);
		const lastSegment = this.scene.road.getLastSegment();
		const lastSegmentTrackPosition = lastSegment.p2.world.z;

		const startLength = startTime * playerSpeed * dlt;
		let lastLength = endTime * playerSpeed * dlt;

		if (startLength >= lastSegmentTrackPosition) { return; }

		if (!endTime) {
			lastLength = lastSegmentTrackPosition;
		}

		let ledgeDrawDistance = distanceLedge;
		let totalTime = this.addTime(startTime, ledgeDrawDistance);

		while (totalTime * playerSpeed * dlt <= lastLength) {
			this.initLedge({
				time: totalTime,
				frame: LEDGE,
			});

			ledgeDrawDistance = this.addTime(ledgeDrawDistance, distanceLedge * this.scene.speedRate);
			totalTime = this.addTime(startTime, ledgeDrawDistance);
		}
	}

	private addTime(t1: number, t2: number) {
		const convertT1 = Number.parseFloat(t1.toFixed(2));
		const convertT2 = Number.parseFloat(t2.toFixed(2));

		return (convertT1 * 100 + convertT2 * 100) / 100;
	}

	private initLedge(configs: any) {
		const { time } = configs;
		const dlt = 1000 * 0.01;
		const playerSpeed = Util.getPlayerSpeed(this.scene.player);
		const trackPosition = playerSpeed * time * dlt + gameSettings.distanceCamToPlayer;
		const segment = this.scene.road.findSegmentByZ(trackPosition);
		const lastSegment = this.scene.road.getLastSegment();

		if (!segment || !segment.visible || trackPosition > lastSegment.p2.world.z) {
			return;
		}

		const ledge = new Ledge(this.scene);
		const percent = Util.percentRemaining(trackPosition, gameSettings.segmentLength);
		const worldY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, percent);
		const ledgeWorldX = Util.interpolate(segment.p1.world.x, segment.p2.world.x, percent);

		ledge.setColor(Util.randomBallColor(!this.lastLedge ? this.scene.player.color : this.lastLedge.color));
		ledge.setSegment(segment);
		ledge.setPercent(percent);
		ledge.setPosition(ledgeWorldX, worldY, trackPosition);

		segment.ledge = ledge;

		this.ledges.add(ledge);
		this.lastLedge = ledge;
	}
}
