import {GameScene} from '../scenes/GameScene';
import {COLORS} from '../constants/BallColor';
import {TEXTURE} from '../constants/CommonType';
import {LEDGE} from '../constants/SpriteName';
import {TrackSegment} from './TrackSegment';
import {gameSettings} from '../config/GameSettings';

export class Ledge {
	public scene: GameScene;
	public sprite: Phaser.GameObjects.Image;
	public color: string;

	private readonly width: number;
	private position: Phaser.Math.Vector3;
	private depth: number = 0;
	private percent: number = 0;
	private segment: TrackSegment;

	constructor(scene: GameScene) {
		this.scene = scene;
		this.sprite = this.scene.add.sprite(0, 0, TEXTURE, LEDGE).setVisible(false);
		this.width = gameSettings.ledgeWidth;
	}

	public setPosition(x: number, y: number, z: number) {
		this.position = new Phaser.Math.Vector3(x, y, z);
	}

	public getPosition() {
		return this.position;
	}

	public setColor(color: string) {
		if (!color) { return; }

		this.color = color;
		// @ts-ignore
		this.sprite.setTint(COLORS[color]);
	}

	public getColor() {
		return this.color;
	}

	public setSegment(segment: TrackSegment) {
		this.segment = segment;
	}

	public getSegment() {
		return this.segment;
	}

	public setPercent(percent: number) {
		this.percent = percent;
	}

	public setDepth(depth: number) {
		this.depth = depth;
	}

	public hide(): void {
		this.sprite.setVisible(false);
	}

	public update(time: number, delta: number) {
		const {
			x, y, w,
		} = this.scene.cameraManager.projectObject({
			x: this.position.x,
			y: this.position.y,
			z: this.position.z,
			width: this.width,
		});

		// Check ball after hill
		this.sprite.setVisible(false);
		this.sprite.setDepth(this.scene.road.segments.length - this.segment.index - this.percent);

		if (this.segment.show && this.segment.visible) {
			this.sprite.setVisible(true);
		}
		this.sprite.setPosition(x, y);
		this.sprite.setDisplaySize(w, w / 10);
	}
}
