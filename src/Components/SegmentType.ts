export const SEGMENT = {
	LENGTH: {
		NONE: 0,
		VERY_SHORT: 8,
		SHORT: 15,
		MEDIUM: 20,
		LONG: 30,
		VERY_LONG: 50,
	},
	CURVE: {
		NONE: 0,
		MINIMAL: 50,
		EASY: 100,
		MEDIUM: 200,
		HARD: 400,
	},
	HILL: {
		NONE: 0,
		VERY_LOW: 2,
		LOW: 4,
		MEDIUM: 6,
		HIGH: 10,
	},
	WIDTH: {
		NORMAL: 1,
		WIDE: 2,
		NARROW: 0.5,
	},
};
