import {GameScene} from '../scenes/GameScene';
import {Util} from './Util';
import {gameSettings} from '../config/GameSettings';
import {BALL, BALL_SHADOW, LEDGE} from '../constants/SpriteName';
import {TEXTURE} from '../constants/CommonType';
import {Ball} from './Ball';

export class BallManager {
	public balls: Set<Ball> = new Set<Ball>();
	public ballObjectPools: Phaser.GameObjects.Group;
	public ballShadowObjectPools: Phaser.GameObjects.Group;

	private readonly scene: GameScene;
	private isResetting: boolean;

	constructor(scene: GameScene) {
		this.scene = scene;
		this.ballObjectPools = new Phaser.GameObjects.Group(this.scene, null, {
			classType: Phaser.GameObjects.Image,
			defaultKey: TEXTURE,
			defaultFrame: BALL,
			maxSize: gameSettings.displayBalls,
		});
		this.ballShadowObjectPools = new Phaser.GameObjects.Group(this.scene, null, {
			classType: Phaser.GameObjects.Image,
			defaultKey: TEXTURE,
			defaultFrame: BALL_SHADOW,
			maxSize: gameSettings.displayBalls,
		});
	}

	public init() {
		const { road: roadConfig, roadRandom: roadRandomConfig } = this.scene.musicConfigs;
		const ballConfigs = roadConfig.filter((config: any) => {
			return !config.isLedge;
		});

		// tslint:disable-next-line:prefer-for-of
		for (let n = 0; n < ballConfigs.length; n++) {
			this.initBall(ballConfigs[n]);
		}

		// tslint:disable-next-line:prefer-for-of
		for (let n = 0; n < roadRandomConfig.length; n++) {
			this.randomRender(roadRandomConfig[n]);
		}
	}

	public reset() {
		this.hideAll();
		this.balls.clear();

		this.init();
	}

	public hideBall(ball: Ball) {
		this.balls.delete(ball);
		ball.hide();
	}

	public hideAll(): void {
		this.balls.forEach( (ball: Ball) => {
			const position = ball.getPosition();
			const segment = ball.getSegment();

			// ball.setActive(false);
			ball.hide();

			if (position.z < this.scene.player.trackPosition) {
				// Remove ball in list
				this.balls.delete(ball);

				// Remove ball on road
				if (segment.balls.size > 0) {
					segment.balls.delete(ball);
				}
			}
		});
	}

	public update(time: number, delta: number) {
		this.hideAll();

		this.updateBalls(time, delta);
	}

	public getBallObjectPools() {
		return this.ballObjectPools.get();
	}

	public getBallShadowObjectPools() {
		return this.ballShadowObjectPools.get();
	}

	private initBall(configs: any) {
		const { time, offset, frame, autoMove, amount, velocity = 0, isGuide } = configs;
		const dlt = 1000 * 0.01;
		const playerSpeed = Util.getPlayerSpeed(this.scene.player);
		const trackPosition = playerSpeed * time * dlt / this.scene.speedRate + gameSettings.distanceCamToPlayer;
		const segment = this.scene.road.findSegmentByZ(trackPosition);

		if (!segment || !segment.visible || segment.ledge) {
			return;
		}

		const percent = Util.percentRemaining(trackPosition, gameSettings.segmentLength);
		const worldCenterX = Util.interpolate(segment.p1.world.x, segment.p2.world.x, percent);
		const ballWorldY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, percent) + gameSettings.ballSize / 2;
		let { colors } = configs;

		const ballPos = [
			worldCenterX + (-gameSettings.roadWidth / 2 + gameSettings.ballMarginRumble + gameSettings.ballSize / 2),
			worldCenterX,
			worldCenterX + (gameSettings.roadWidth / 2 - gameSettings.ballMarginRumble - gameSettings.ballSize / 2),
		];

		// Check if track position > amount segment => Return
		if (trackPosition > this.scene.road.segments.length * gameSettings.segmentLength) {
			return;
		}

		// Random color
		if (colors === 'RANDOM') {
			colors = this.getListRandomColors(amount);
		}

		// Set info for amount balls
		for (let n = 0; n < amount; n++) {
			const ball = new Ball(this.scene, this);

			// Get color for ball
			// tslint:disable-next-line:no-shadowed-variable
			const frame = this.getFrame(colors[n], trackPosition);

			if (!frame) {
				continue;
			}

			// Get offset for ball
			const ballWorldX = this.getWorldX(offset, ballPos, n);

			// Set auto move for ball
			if (autoMove) {
				ball.setAutoMove(true);
			}

			// Set is guide for ball
			if (isGuide) {
				this.setGuide(ball, colors[n]);
			}

			ball.setFrame(frame);
			ball.setPercent(percent);
			ball.setPosition(ballWorldX, ballWorldY, trackPosition);
			ball.setSegment(segment);
			ball.setVelocity(velocity);
			ball.index = this.balls.size;

			this.balls.add(ball);
			segment.balls.add(ball);
		}

	}

	private randomRender(config: any) {
		const { distanceBall, endTime, startTime, colors = 'RANDOM' } = config;
		const dlt = 1000 * 0.01;
		const playerSpeed = Util.getPlayerSpeed(this.scene.player);
		const lastSegment = this.scene.road.getLastSegment();
		const lastSegmentTrackPosition = lastSegment.p2.world.z;

		const startLength = startTime * playerSpeed * dlt;
		let lastLength = endTime * playerSpeed * dlt;

		if (startLength >= lastSegmentTrackPosition) { return; }

		if (!endTime) {
			lastLength = lastSegmentTrackPosition;
		}

		let ballDrawDistance = 0;
		let totalTime = this.addTime(startTime, ballDrawDistance);

		while (totalTime * playerSpeed * dlt <= lastLength) {
			let amount = 3 || Util.getRndInteger(1, 3);

			if (Array.isArray(colors)) {
				amount = colors.length;
			}

			const frame = BALL;

			let offset = 'RANDOM';
			let autoMove = false;

			if (amount === 1) {
				autoMove = Util.getRndInteger(0, 1) === 1;
			} else {
				offset =  amount < 3 ? 'RANDOM' : 'DEFAULT';
			}

			this.initBall({
				time: totalTime,
				offset,
				frame,
				autoMove,
				amount,
				colors,
			});

			ballDrawDistance =  this.addTime(ballDrawDistance, distanceBall * this.scene.speedRate);
			totalTime = this.addTime(startTime, ballDrawDistance);
		}
	}

	private addTime(t1: number, t2: number) {
		const convertT1 = Number.parseFloat(t1.toFixed(2));
		const convertT2 = Number.parseFloat(t2.toFixed(2));

		return (convertT1 * 100 + convertT2 * 100) / 100;
	}

	private updateBalls(time: number, delta: number) {
		const start = this.scene.cameraManager.baseSegment.p1.world.z || 0;
		const deltaPosition = gameSettings.drawDistance * gameSettings.segmentLength;
		const end = Phaser.Math.Clamp(deltaPosition, start + deltaPosition, this.scene.road.segments.length * gameSettings.segmentLength);

		this.balls.forEach( (ball: Ball) => {
			const ballSegment = ball.getSegment();
			if (start > ballSegment.p1.world.z || ballSegment.p2.world.z > end) {
				return;
			}

			const ballDrawer = this.getBallObjectPools();
			ball.setActive(true);
			ball.setBallDrawer(ballDrawer);
			ball.update(time, delta);
		});
	}

	private getListRandomColors(amount: number) {
		const colors = [];

		while (colors.length < amount) {
			colors.push(Util.randomBallColor());
			if (colors.length === amount) {
				const idx = Util.getRndInteger(0, amount - 1);
				colors[idx] = 'PLAYER_COLOR';
			}
		}

		return colors;
	}

	private getFrame(color: string, trackPosition: number) {
		let mainColor = color;
		const lastLedge = this.scene.ledgeManager.getLastLedge(trackPosition);
		const playerColor = !lastLedge ? this.scene.player.getFrame() : lastLedge.getColor();

		switch (color) {
			case 'PLAYER_COLOR':
				mainColor = playerColor;
				break;

			case 'RANDOM':
				mainColor = Util.randomBallColor(playerColor);
				break;

			case 'NULL':
				mainColor = null;
				break;
		}

		return mainColor;
	}

	private getWorldX(offset: any, ballPos: any[], index: number) {
		let ballWorldX = 0;

		switch (offset) {
			case 'RANDOM':
				ballWorldX = ballPos[Util.getRndInteger(0, 2)];
				break;

			default:
				ballWorldX = ballPos[index];
				break;
		}

		return ballWorldX;
	}

	private setGuide(ball: Ball, color: string) {
		if (!color || color !== 'PLAYER_COLOR') { return; }

		ball.setIsGuide(true);
	}
}
