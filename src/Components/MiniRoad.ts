import {TEXTURE} from '../constants/CommonType';
import {TIME_LINE_BLUE, TIME_LINE_PINK, TIME_LINE_STAR} from '../constants/SpriteName';

export class MiniRoad {
	private scene: any;
	private x: number;
	private y: number;
	private line: any;
	private lineLoading: any;
	private timeline: any;

	constructor(scene: any) {
		this.scene = scene;
		this.x = 50;
		this.y = 100;
	}

	public init() {
		this.line = this.scene.add.image(this.x, this.y, TEXTURE, TIME_LINE_BLUE).setOrigin(0, 0).setDepth(1);
		this.lineLoading = this.scene.add.image(this.x, this.y + this.line.displayHeight, TEXTURE, TIME_LINE_PINK).setOrigin(0, 1).setDepth(2);
		this.timeline = this.scene.add.image(this.x, this.y, TEXTURE, TIME_LINE_STAR).setOrigin(0, 0).setDepth(3);
		// this.sprite.setScale(3);
	}

	public draw() {
		this.lineLoading.setScale(1, this.scene.player.trackPosition / this.scene.road.trackLength);
	}
}
