import { GameScene } from '../scenes/GameScene';
import { gameSettings } from '../config/GameSettings';
import { Util } from './Util';
import { RAPID } from '../constants/RoadType';
import {
	DEATH,
	DYING,
	FLYING,
	JUMPING, RESPAWNED, RESPAWNED_RUNNING, RESPAWNING,
	RUNNING,
	STANDING,
	STOPPED,
	STOPPING,
} from '../constants/PlayerStatus';
import { BallTail } from './BallTail';
import { PlayerAnimation } from '../particals/PlayerAnimation';
import { COLORS } from '../constants/BallColor';
import {TEXTURE} from '../constants/CommonType';
import {BALL_FRAMES, BALL_SHADOW} from '../constants/SpriteName';
import {TrackSegment} from './TrackSegment';
import TurnController from './PlayerBehaviors/TurnController';
import JumpBehavior from './PlayerBehaviors/JumpBehavior';
import FlyBehavior from './PlayerBehaviors/FlyBehavior';
import CollisionBehavior from './PlayerBehaviors/CollisionBehavior';
import DieBehavior from './PlayerBehaviors/DieBehavior';
import RespawnBehavior from './PlayerBehaviors/RespawnBehavior';

class Player {

	public get x(): number { return this.position.x; }
	public set x(x: number) {
		this.position.x = x;
	}

	public get y(): number { return this.position.y; }
	public set y(y: number) {
		this.position.y = y;
	}

	public get z(): number { return this.position.z; }
	public set z(z: number) {
		this.position.z = z;
	}
	public position: Phaser.Math.Vector3;
	public minPositionX: number;
	public maxPositionX: number;
	public positionY: number;
	public scene: GameScene;
	public model: any;
	public engineSound: Phaser.Sound.WebAudioSound;
	public tireScreechSound: Phaser.Sound.WebAudioSound;
	public turn: number;
	public pitch: number;
	public speed: number;
	public trackPosition: number;
	public accelerating: boolean = false;
	public screeching: boolean = false;
	public image: Phaser.GameObjects.Image;
	public lane: number;
	public color: string;
	public frame: string;
	public tail: any;
	public anim: any;
	public shadow: any;
	public status: string; // JUMP, FLY, STANDING, RUNNING, DYING, DEAD, RESPAWNED >> RUNNING
	public statusUpDown: string; // UP, DOWN.
	public width: number;
	public playerSegment: TrackSegment;
	public playerPercent: number;
	public turnController: TurnController;
	public jumpBehavior: JumpBehavior;
	public flyBehavior: FlyBehavior;
	public collisionBehavior: CollisionBehavior;
	public dieBehavior: DieBehavior;
	public respawnBehavior: RespawnBehavior;
	public respawnState: string;

	constructor(scene: GameScene, sprite: string) {
		this.position = new Phaser.Math.Vector3(0, 0, 0);
		this.scene = scene;
		this.lane = 1;
		this.turn = 0;
		this.pitch = 0;
		this.speed = 0;
		this.trackPosition = 0;
		this.width = gameSettings.playerWidth;
		this.status = STANDING;

		this.image = this.scene.add.image(this.scene.scale.gameSize.width / 2, 0, TEXTURE, sprite);
		this.image.setDepth(1000 || this.scene.road.segments.length + 1);

		//
		this.shadow = this.scene.add.image(0, 0, TEXTURE, BALL_SHADOW);
		this.shadow.setDepth(this.image.depth - 1);

		//
		this.tail = new BallTail(scene, this);
		this.anim = new PlayerAnimation(scene, this, this.image);

		// move controller
		this.turnController = new TurnController(this.scene, this);

		// behaviors
		this.flyBehavior = new FlyBehavior(this.scene, this);
		this.jumpBehavior = new JumpBehavior(this.scene, this);
		this.collisionBehavior = new CollisionBehavior(this.scene, this);
		this.dieBehavior = new DieBehavior(this.scene, this);
		this.respawnBehavior = new RespawnBehavior(this.scene, this);
	}

	public setFrame(frame: string) {
		this.frame = frame;

		// @ts-ignore
		this.image.setFrame(BALL_FRAMES[frame]);
	}

	public getFrame() {
		return this.frame;
	}

	public move(time: number, delta: number) {
		if (this.isDeath() || this.isStanding()) { return; }

		this.turnController.update(time, delta);
	}

	public bounce(time: number, alpha: number) {
		this.jumpBehavior.start(time, alpha, 1);
	}

	public run() {
		this.speed = gameSettings.startPlayerSpeed;
		this.status = RUNNING;
		this.shadow.setVisible(false);
		this.tail.start();
	}

	public dying() {
		this.scene.musicManager.pause();
		this.speed = 0;
		this.status = DYING;
		this.tail.stop();
	}

	public getStatus() {
		return this.status;
	}

	public reset() {
		this.trackPosition = 0;
		this.speed = 0;
		this.status = STANDING;
		this.tail.reset();

		this.image.setVisible(true);
		this.image.setActive(true);
		this.image.setAlpha(1);
	}

	public continue() {
		this.respawnBehavior.respawn();
	}

	public runAfterRespawned() {
		this.respawnBehavior.run();
	}

	public drawPlayer(time: number, delta: number) {
		const {
			x, y, w,
		} = this.scene.cameraManager.projectObject({
			x: this.x,
			y: this.y,
			z: this.z,
			width: this.width,
		});

		this.image.setPosition(x, y);

		this.image.setDisplaySize(w, w);

		this.shadow.setPosition(this.image.x, this.image.y + this.image.displayHeight * 2 / 3);

		this.tail.update(time, delta);
	}

	public updateTrackPosition(time: number, delta: number) {
		const dlt = delta * 0.01;

		this.trackPosition = Util.increase(this.trackPosition, dlt * this.speed, this.scene.road.trackLength);
		this.playerSegment = this.scene.road.findSegmentByZ(this.trackPosition + gameSettings.distanceCamToPlayer);
		this.playerPercent = Util.percentRemaining(this.trackPosition + gameSettings.distanceCamToPlayer, gameSettings.segmentLength);

		this.checkFly(time, delta);
	}

	public update(time: number, delta: number) {
		if (this.isDeath()) {
			return;
		}

		this.positionUpdate();

		this.performBehavior(time, delta);

		this.behaviorUpdate(time, delta);

		this.animUpdate();
	}

	public playEngineSound(): void {
		if (this.speed > 0 && this.engineSound.isPlaying) {
			this.engineSound.setDetune( this.speed * 1.25 );
			this.engineSound.setVolume( 0.7 + Phaser.Math.Clamp(this.speed * 0.0001, 0, 0.2) );
		}
	}

	public tireScreech(play = false): void {
		if (play && !this.tireScreechSound.isPlaying) {
			this.tireScreechSound.play();
		} else {
			this.tireScreechSound.stop();
		}
	}

	public getHeight() {
		let height = 0;

		if (this.status === FLYING)  {
			height = this.flyBehavior.getHeight();
		} else if (this.status === JUMPING) {
			height = this.jumpBehavior.getHeight();
		}

		return height;
	}

	public getMaxHeight() {
		let height = 0;

		if (this.status === FLYING)  {
			height = this.flyBehavior.getMaxHeight();
		} else if (this.status === JUMPING) {
			height = this.jumpBehavior.getMaxHeight();
		}

		return height;
	}

	private isDeath() {
		// DEATH is game over
		return this.status === DEATH;
	}

	private isStanding() {
		return this.status === STANDING;
	}

	private positionUpdate() {
		const { p1, p2 } = this.playerSegment;

		const playerWorldX = Util.interpolate(p1.world.x, p2.world.x, this.playerPercent);
		const min = playerWorldX - gameSettings.roadWidth / 2 + gameSettings.playerWidth / 2;
		const max = playerWorldX + gameSettings.roadWidth / 2 - gameSettings.playerWidth / 2;

		this.position.x = Phaser.Math.Clamp(this.position.x, min, max);
		this.position.z = this.trackPosition + gameSettings.distanceCamToPlayer;
		this.position.y = Util.interpolate(p1.world.y, p2.world.y, this.playerPercent) + gameSettings.playerWidth / 2;
	}

	private behaviorUpdate(time: number, delta: number) {
		switch (this.status) {
			case JUMPING:
				this.jumpBehavior.update(time, delta);
				break;

			case FLYING:
				this.flyBehavior.update(time, delta);
				break;

			case RUNNING:
				break;

			case DYING:
				this.dieBehavior.update(time, delta);
				break;
		}

		this.respawnBehavior.update(time, delta);
	}

	private animUpdate() {
		this.anim.update(this.image);
	}

	private ballOverlap(playerSegment: any, delta: number) {
		if (this.isRespawning()) { return; }

		// collision check with balls if on road
		if (playerSegment.balls.size) {
			for (const ball of playerSegment.balls) {
				this.collisionBehavior.collisionBall(ball, delta);
			}
		}
	}

	private isRespawning() {
		return this.respawnBehavior.getState() === RESPAWNED_RUNNING;
	}

	private ledgeOverlap(playerSegment: any, time: number) {
		// collision with ledges
		if (playerSegment.ledge && this.status !== FLYING) {
			this.jumpBehavior.start(time, gameSettings.ledgeAlpha);
			this.setFrame(playerSegment.ledge.color);
		}
	}

	private exitDoorOverlap(playerSegment: any, time: number) {
		if (playerSegment.exitDoor) {
			this.scene.reset();

			this.trackPosition = 0;
		}
	}

	private checkFly(time: number, delta: number) {
		if (this.playerSegment.type !== RAPID || !this.playerSegment.isHighest) {
			return;
		}

		const dlt = delta * 0.01;
		const nextTrackPosition = Util.increase(this.trackPosition, dlt * this.speed, this.scene.road.trackLength) + gameSettings.distanceCamToPlayer;

		if (nextTrackPosition >= this.playerSegment.p2.world.z) {
			this.flyBehavior.start(time, gameSettings.flyAlpha);
		}
	}

	private performBehavior(time: number, delta: number) {
		const playerSegment = this.playerSegment;

		if (!playerSegment.visible) {
			return;
		}

		this.ballOverlap(playerSegment, delta);

		this.ledgeOverlap(playerSegment, time);

		this.exitDoorOverlap(playerSegment, time);
	}
}

export default Player;
