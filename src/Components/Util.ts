import Player from './Player';
import { gameSettings } from '../config/GameSettings';
import { STRING_COLORS } from '../constants/BallColor';
import {Ball} from './Ball';

export class Util {
	public static getDevicePixelRatio() {
		// return 1;
		return window.devicePixelRatio === 1 ? 1 : 2;
	}

	public static randomBallColor(exColor?: string) {
		if (!STRING_COLORS) { return ; }

		const colors = [...STRING_COLORS];

		if (exColor) {
			const idx = colors.findIndex((c) => {
				return exColor === c;
			});

			colors.splice(idx, 1);
		}

		const index = this.getRndInteger(0, colors.length - 1);
		return colors[index];
	}

	public static rumbleWidth(projectedRoadWidth: number, lanes: number): number {
		return projectedRoadWidth / Math.max(24, 2 * lanes);
	}

	public static laneMarkerWidth(projectedRoadWidth: number, lanes: number): number {
		return projectedRoadWidth / Math.max(32, 8 * lanes);
	}

	public static increase(start: number, increment: number, max: number): number { // with looping
		let result = start + increment;
		while (result >= max) {
			result -= max;
		}

		while (result < 0) {
			result += max;
		}
		return result;
	}

	public static accelerate(current: number, accel: number, delta: number): number {
		return current + (accel * delta);
	}

	public static interpolate(a: number, b: number, percent: number): number {
		return a + (b - a) * percent;
	}

	public static easeIn(a: number, b: number, percent: number): number {
		return a + (b - a) * Math.pow(percent, 2);
	}

	public static easeOut(a: number, b: number, percent: number): number {
		return a + (b - a) * (1 - Math.pow(1 - percent, 2));
	}

	public static easeInOut(a: number, b: number, percent: number): number {
		return a + (b - a) * ((-Math.cos(percent * Math.PI) / 2) + 0.5);
	}

	public static easeInSine(a: number, b: number, percent: number): number {
		if (b > 0) {
			return a + (b - a) * (Math.pow(percent, 5));
		}

		return a + b;
	}

	public static percentRemaining(n: number, total: number): number {
		return (n % total) / total;
	}

	public static toInt(obj: any, def: any): number {
		if (obj !== null) {
			const x = parseInt(obj, 10);
			if (!isNaN(x)) {
				return x;
			}
		}

		return Util.toInt(def, 0);
	}

	public static overlapPlayer(player: Player, ball: Ball, delta: number): boolean {
		// const rect = new Phaser.Geom.Rectangle();
		// const overlaps = target.getBounds(rect).contains(player.image.x - player.image.displayWidth / 2, player.image.y - player.image.displayHeight / 2)
		// 			  || target.getBounds(rect).contains(player.image.x + player.image.displayWidth / 2, player.image.y - player.image.displayHeight / 2)
		// 			  || target.getBounds(rect).contains(player.image.x - player.image.displayWidth / 2, player.image.y + player.image.displayHeight / 2)
		// 			  || target.getBounds(rect).contains(player.image.x + player.image.displayWidth / 2, player.image.y + player.image.displayHeight / 2);
		//
		// return overlaps;

		const playerPosition = player.position;
		const ballPosition = ball.getPosition();
		const sPlayerMove = delta * 0.01 * player.speed;
		let overlapX = false;
		let overlapY = false;
		let overlapZ = false;

		//
		const playerX3DLeft = playerPosition.x - gameSettings.playerWidth / 2;
		const playerX3DRight = playerPosition.x + gameSettings.playerWidth / 2;
		const playerY3DTop = playerPosition.y - gameSettings.playerWidth / 2;
		const playerY3DBot = playerPosition.y + gameSettings.playerWidth / 2;
		const playerZ3DBefore = playerPosition.z - gameSettings.playerWidth / 2;
		const playerZ3DAfter = playerPosition.z + gameSettings.playerWidth / 2;

		//
		const ballX3DLeft = ballPosition.x - gameSettings.playerWidth / 2;
		const ballX3DRight = ballPosition.x + gameSettings.playerWidth / 2;
		const ballY3DTop = ballPosition.y - gameSettings.playerWidth / 2;
		const ballY3DBot = ballPosition.y + gameSettings.playerWidth / 2;
		const ballZ3DBefore = ballPosition.z - gameSettings.playerWidth / 2;
		const ballZ3DAfter = ballPosition.z + gameSettings.playerWidth / 2;

		//
		if ((playerX3DLeft <= ballX3DLeft && ballX3DLeft <= playerX3DRight) ||
			(playerX3DLeft <= ballX3DRight && ballX3DRight <= playerX3DRight)) {
			overlapX = true;
		}

		if ((playerY3DTop <= ballY3DTop && ballY3DTop <= playerY3DBot) ||
			(playerY3DTop <= ballY3DBot && ballY3DBot <= playerY3DBot)) {
			overlapY = true;
		}

		if ((playerZ3DBefore <= ballZ3DBefore && ballZ3DBefore <= playerZ3DAfter) ||
			(playerZ3DBefore <= ballZ3DAfter && ballZ3DAfter <= playerZ3DAfter) ||
			(playerPosition.z < ballPosition.z && ballPosition.z <= (playerPosition.z + sPlayerMove))) {
			overlapZ = true;
		}

		return overlapX && overlapY && overlapZ;
	}

	public static overlapSprite(a: Phaser.GameObjects.Sprite, b: Phaser.GameObjects.Sprite): boolean {
		const aBounds = a.getBounds();
		const bBounds = b.getBounds();

		return Phaser.Geom.Rectangle.Overlaps(aBounds, bBounds);
	}

	public static getRndInteger(min: number, max: number) {
		return Math.floor(Math.random() * (max - min + 1) ) + min;
	}

	public static radian(degree: number) {
		return degree * Math.PI / 180;
	}

	public static tan(degree: number) {
		return Math.tan(this.radian(degree));
	}

	public static sin(degree: number) {
		return Math.sin(this.radian(degree));
	}

	public static cos(degree: number) {
		return Math.cos(this.radian(degree));
	}

	public static getJumpX(v0: number, alpha: number, time: number) {
		return v0 * this.cos(alpha) * time;
	}

	public static getJumpY(v0: number, alpha: number, time: number, gravity?: number) {
		return (v0 * this.sin(alpha)) * time - 1 / 2 * (gravity || gameSettings.gravity) * Math.pow(time, 2);
	}

	public static getGoDownY(maxHeight: number, time: number) {
		return maxHeight - 1 / 2 * gameSettings.gravity * Math.pow(time, 2);
	}

	public static getGetVyByTime(v0: number, alpha: number, time: number) {
		return v0 * this.sin(alpha) - gameSettings.gravity * time;
	}

	public static getJumpMaxHeight(v0: number, alpha: number, gravity?: number) {
		return (Math.pow(v0, 2) * Math.pow(this.sin(alpha), 2)) / (2 * (gravity || gameSettings.gravity));
	}

	public static getTimeJumpMaxHeight(v0: number, alpha: number, gravity?: number) {
		return v0 * this.sin(alpha) / gravity;
	}

	public static getHillAlpha(length: number, height: number) {
		return 180 / Math.PI * Math.atan(height / length);
	}

	public static getTimer(time: number) {
		const f = 20;
		const dlt = f * 0.01;
		const v = gameSettings.maxSpeed / 3;
		const s = v * dlt; // quang duong xe di
		return (time * 1000 / f) * s / v; // 16 is delta time cua game
	}

	public static getPlayerSpeed(player: any) {
		if (!player.speed) { return gameSettings.startPlayerSpeed; }
		return player.speed;
	}

	public static getDistance(h: number, z: number) {
		return Math.sqrt(h * h + z * z);
	}

	public static getAngleBallTail(x: number, x1: number) {
		return 1 / Math.tan(180 / Math.PI * Math.atan(gameSettings.lightPosition / Math.abs(x1 - x)) * Math.PI / 180 );
	}

	public static ease(current: number, target: number, ease: number) {
		return current + (target - current) * ( ease || 0.2 );
	}
}
