import {GameScene} from '../scenes/GameScene';
import {Util} from './Util';
import {gameSettings} from '../config/GameSettings';
import {TEXTURE} from '../constants/CommonType';
import {EXIT_DOOR} from '../constants/SpriteName';
import {TrackSegment} from './TrackSegment';

export class ExitDoor {
	public scene: GameScene;
	public sprite: Phaser.GameObjects.Image;
	public percent: number = 0;
	private position: Phaser.Math.Vector3;
	private segment: TrackSegment;

	constructor(scene: GameScene) {
		this.scene = scene;
		this.sprite = this.scene.add.sprite(0, 0, TEXTURE, EXIT_DOOR).setVisible(false).setDepth(10);
	}

	public init() {
		const trackPosition = (this.scene.road.segments.length - 1) * gameSettings.segmentLength;
		this.segment = this.scene.road.findSegmentByZ(trackPosition);
		const percent = Util.percentRemaining(trackPosition, gameSettings.segmentLength);

		const worldX = Util.interpolate(this.segment.p1.world.x, this.segment.p2.world.x, percent);
		const worldY = Util.interpolate(this.segment.p1.world.y, this.segment.p2.world.y, percent);

		this.segment.exitDoor = this;
		this.position = new Phaser.Math.Vector3(worldX, worldY, trackPosition);

	}

	public hide() {
		this.sprite.setVisible(false);
	}

	public update(time: number, delta: number) {
		if (!this.segment.show) { return; }

		const {
			x, y, w,
		} = this.scene.cameraManager.projectObject({
			x: this.position.x,
			y: this.position.y,
			z: this.position.z,
			width: gameSettings.exitDoorSize,
		});

		this.sprite.setVisible(true);
		this.sprite.setPosition(x, y);
		this.sprite.setDisplaySize(w, w);
	}
}
