import { TrackSegment } from './TrackSegment';
import { gameSettings } from '../config/GameSettings';
import { Util } from './Util';
import { SEGMENT } from './SegmentType';
import { GameScene } from '../scenes/GameScene';
import { HILL, STRAIGHT, CURVE, RAPID } from '../constants/RoadType';
import Road3D from './Road3D';

export class Road {
	public scene: GameScene;
	public segments: TrackSegment[];
	public trackLength: number;
	public maxY: number;

	private readonly road3D: Road3D;
	private segmentMaxY: TrackSegment;

	constructor(scene: GameScene) {
		this.scene = scene;
		this.segments = [];
		this.trackLength = 0;
		this.road3D = new Road3D(scene, 0, 0, 'segment');
		scene.add.existing(this.road3D);
	}

	public init(): void {
		this.segments = [];

		this.addStraight(SEGMENT.LENGTH.LONG);

		this.addCurve(SEGMENT.LENGTH.LONG, -SEGMENT.CURVE.HARD, -SEGMENT.HILL.VERY_LOW);
		this.addCurve(SEGMENT.LENGTH.LONG, SEGMENT.CURVE.HARD, +SEGMENT.HILL.VERY_LOW);

		this.addHill(SEGMENT.LENGTH.SHORT, SEGMENT.HILL.LOW);
		this.addHill(SEGMENT.LENGTH.SHORT, -SEGMENT.HILL.LOW);

		this.addStraight(SEGMENT.LENGTH.VERY_SHORT);

		this.addHill(SEGMENT.LENGTH.SHORT, SEGMENT.HILL.VERY_LOW);
		this.addHill(SEGMENT.LENGTH.SHORT, -SEGMENT.HILL.VERY_LOW);

		this.addStraight(SEGMENT.LENGTH.SHORT);

		this.addRapid(SEGMENT.LENGTH.SHORT, SEGMENT.HILL.MEDIUM);
		this.addRapid(SEGMENT.LENGTH.VERY_SHORT, -SEGMENT.HILL.MEDIUM, false);

		this.addStraight(SEGMENT.LENGTH.MEDIUM);

		this.addCurve(SEGMENT.LENGTH.SHORT, -SEGMENT.CURVE.HARD, SEGMENT.HILL.LOW);
		this.addCurve(SEGMENT.LENGTH.SHORT, SEGMENT.CURVE.HARD, -SEGMENT.HILL.LOW);

		this.addCurve(SEGMENT.LENGTH.SHORT, -SEGMENT.CURVE.MEDIUM, SEGMENT.HILL.LOW);
		this.addCurve(SEGMENT.LENGTH.SHORT, SEGMENT.CURVE.MEDIUM, -SEGMENT.HILL.LOW);

		this.addHill(SEGMENT.LENGTH.SHORT, SEGMENT.HILL.MEDIUM);
		this.addHill(SEGMENT.LENGTH.SHORT, -SEGMENT.HILL.MEDIUM);

		this.addStraight(SEGMENT.LENGTH.VERY_SHORT);

		this.addCurve(SEGMENT.LENGTH.SHORT, -SEGMENT.CURVE.MINIMAL, SEGMENT.HILL.MEDIUM);
		this.addCurve(SEGMENT.LENGTH.SHORT, SEGMENT.CURVE.MINIMAL, -SEGMENT.HILL.MEDIUM);

		this.addStraight(SEGMENT.LENGTH.SHORT);

		this.addRapid(SEGMENT.LENGTH.MEDIUM, SEGMENT.HILL.MEDIUM);
		this.addRapid(SEGMENT.LENGTH.VERY_SHORT, -SEGMENT.HILL.MEDIUM, false);

		this.addStraight(SEGMENT.LENGTH.VERY_LONG);

		this.trackLength = this.segments.length * gameSettings.segmentLength;

		this.road3D.create();
		this.road3D.setSegments(this.segments);
		this.road3D.setPlayer(this.scene.player);
	}

	public addRoadSegment(curve: number, y: number, config: any): void {
		const beforeSegment = this.segments.length > 0 ?
			this.segments[this.segments.length - 1] : {p2: {world: {x: 0, y: 0, z: 0}}};

		const { p2 } = beforeSegment;
		const segment = new TrackSegment(
			this.scene,
			this.segments.length,
			curve,
			p2.world.x,
			p2.world.x + curve,
			y,
			this.getLastSegmentYPos(),
			config,
		);
		this.segments.push(segment);
	}

	public addStraight(num: number = SEGMENT.LENGTH.MEDIUM, visible?: boolean): void {
		this.addRoad(num, num, num, 0, 0, {
			rapid: false,
			type: STRAIGHT,
			visible: visible === undefined ? true : visible,
		});
	}

	public addCurve(num: number = SEGMENT.LENGTH.MEDIUM, curve: number = SEGMENT.CURVE.MEDIUM, height: number = SEGMENT.HILL.NONE, visible?: boolean): void {
		this.addRoad(num, num, num, curve, height, {
			rapid: false,
			type: CURVE,
			visible: visible === undefined ? true : visible,
		});
	}

	public addHill(num: number = SEGMENT.LENGTH.MEDIUM, height: number = SEGMENT.HILL.NONE, visible?: boolean): void {
		this.addRoad(num, num, num, 0, height, {
			rapid: false,
			type: HILL,
			visible: visible === undefined ? true : visible,
		});
	}

	public addRapid(num: number = SEGMENT.LENGTH.MEDIUM, height: number = SEGMENT.HILL.NONE, visible?: boolean): void {
		this.addRoadRapid(num, num, num, 0, height, {
			rapid: true,
			type: RAPID,
			visible: visible === undefined ? true : visible,
		});
	}

	public addRoad(enter: number, hold: number, leave: number, curve: number, y: number, config?: any): void {
		const { type, visible } = config || {};
		const upOrDownHill = y >= 0 ? true : false;

		const startY = this.getLastSegmentYPos();
		const endY = startY + Util.toInt(y, 0) * gameSettings.segmentLength;
		const totalLength = enter + hold + leave;

		const alpha = (type === HILL || type === RAPID) ? Util.getHillAlpha(totalLength * gameSettings.segmentLength, endY - startY) : 0;

		for (let n = 0; n < enter; n++) {
			this.addRoadSegment(Util.easeIn(0, curve, n / enter),
				Util.easeInOut(startY, endY, n / totalLength),
				{ type, alpha, visible: !!visible, upOrDownHill });
		}

		for (let n = 0; n < hold; n++) {
			this.addRoadSegment(curve,
				Util.easeInOut(startY, endY, (enter + n) / totalLength),
				{ type, alpha, visible: !!visible, upOrDownHill });
		}

		for (let n = 0; n < leave; n++) {
			const isHighest = (type === RAPID) && n === leave - 1 && y > 0;

			this.addRoadSegment(Util.easeInOut(curve, 0, n / leave),
				Util.easeInOut(startY, endY, (enter + hold + n) / totalLength),
				{ type, alpha, visible: !!visible, isHighest, upOrDownHill });
		}
	}

	public addRoadRapid(enter: number, hold: number, leave: number, curve: number, y: number, config?: any): void {
		const { type, visible } = config || {};
		const upOrDownHill = y >= 0 ? true : false;

		const startY = this.getLastSegmentYPos();
		const endY = startY + Util.toInt(y, 0) * gameSettings.segmentLength;
		const totalLength = enter + hold + leave;

		const alpha = (type === HILL || type === RAPID) ? Util.getHillAlpha(totalLength * gameSettings.segmentLength, endY - startY) : 0;

		for (let n = 0; n < enter; n++) {
			this.addRoadSegment(Util.easeIn(0, curve, n / enter),
				Util.easeInSine(startY, endY, n / totalLength),
				{ type, alpha, visible: !!visible, upOrDownHill });
		}

		for (let n = 0; n < hold; n++) {
			this.addRoadSegment(curve,
				Util.easeInSine(startY, endY, (enter + n) / totalLength),
				{ type, alpha, visible: !!visible, upOrDownHill });
		}

		for (let n = 0; n < leave; n++) {
			const isHighest = (type === HILL || type === RAPID) && n === leave - 1 && y > 0;

			this.addRoadSegment(Util.easeInOut(curve, 0, n / leave),
				Util.easeInSine(startY, endY, (enter + hold + n) / totalLength),
				{ type, alpha, visible: !!visible, isHighest, upOrDownHill });
		}
	}

	public getLastSegmentYPos(): number {
		const lastSegment = this.getLastSegment();
		return lastSegment ? lastSegment.p2.world.y : 0;
	}

	public getLastSegment(): TrackSegment {
		return this.segments.length > 0 ? this.segments[this.segments.length - 1] : null;
	}

	public findSegmentByZ(z: number): TrackSegment {
		const index = Math.floor(z / gameSettings.segmentLength) % this.segments.length;
		return this.segments[index];
	}

	public getFirstSegment() {
		const gameHeight = this.scene.scale.gameSize.height;

		return this.segments.find((segment: TrackSegment) => {
			return Math.abs(segment.p1.screen.y) < gameHeight;
		});
	}

	public reset() {
		if (this.segments.length <= 0) { return; }

		this.segments.forEach((segment: TrackSegment) => {
			segment.reset();
		});
	}

	public update(time: number, delta: number) {
		this.hideAll();

		this.updateSegment();

		this.road3D.update(time, delta);
	}

	private hideAll() {
		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < this.segments.length; i++) {
			this.segments[i].show  = false;
		}
	}

	private updateSegment() {
		this.maxY = this.scene.scale.gameSize.height;
		this.segmentMaxY = this.scene.cameraManager.baseSegment;
		const segmentLength = this.scene.road.segments.length;

		const start = this.scene.cameraManager.baseSegment.index;
		const end = (start + gameSettings.shouldDrawBallSegments) < segmentLength ?
			(start + gameSettings.shouldDrawBallSegments) : segmentLength;

		for (let i = start; i < end; i++) {
			const segment = this.scene.road.segments[i];

			const { y: y2 } = this.scene.cameraManager.projectObject({
				x: segment.p2.world.x,
				y: segment.p2.world.y,
				z: segment.p2.world.z,
				width: 0,
			});

			segment.show = false;

			if (y2 < 0 || y2 > this.maxY) {
				continue;
			}

			if (y2 <= this.maxY) {
				segment.show = true;
				this.maxY = y2;
				this.segmentMaxY = segment;
			}

			if (this.segmentMaxY.p2.world.z >= segment.p2.world.z) {
				segment.show = true;
			}
		}
	}
}
