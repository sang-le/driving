import {gameSettings} from '../config/GameSettings';
import { Util } from './Util';
import Player from './Player';
import { FLYING } from '../constants/PlayerStatus';

export class CameraManager {
	public position: { x: any; y: any; z: number };
	public player: Player;
	public targetPosition: any;
	public baseSegment: any;
	private deltaX: number = 0;
	private targetDeltaX: number = 0;

	private scene: any;
	private readonly gameWidth: number;
	private readonly gameHeight: number;
	private projectionMatrix: any;
	private viewProjectionMatrix: any;
	private zNear = 1;
	private zFar = 2000000;
	private readonly fieldOfViewRadians: number = 60;

	constructor(scene: any) {
		this.scene = scene;
		this.position = new Phaser.Math.Vector3(0, 0, 0);
		this.gameWidth = scene.scale.gameSize.width;
		this.gameHeight = scene.scale.gameSize.height;
		this.fieldOfViewRadians =  gameSettings.fieldOfView * Math.PI / 180;

		this.setMatrices();
	}

	public setTarget(target: any) {
		this.player = target;
		this.targetPosition = [target.x, target.y, target.z + gameSettings.camTargetZPlus];
	}

	/**
	 * x, y, z: Center Point of given object
	 * width: Object Width in 3D World
	 *
	 * @param obj {x, y, z, width}
	 */
	public projectObject(obj: any) {
		const point3d = [obj.x, obj.y, obj.z];

		// transform world to clipping coordinates
		const point2d = m4.transformPoint(this.viewProjectionMatrix, point3d);
		const winX =  (( point2d[0] + 1 ) / 2.0) * this.gameWidth;

		// we calculate -point3D.getY() because the screen Y axis is
		// oriented top->down
		const winY = (( 1 - point2d[1] ) / 2.0) * this.gameHeight;

		// calculate half width
		const vec3 = [obj.x - obj.width / 2, obj.y, obj.z];
		const vec2 = m4.transformPoint(this.viewProjectionMatrix, vec3);
		const winX2 =  (( vec2[0] + 1 ) / 2.0) * this.gameWidth;
		const w = Math.abs(winX2 - winX) * 2;

		return {x: winX, y: winY, w};
	}

	public update(time: number, delta: number) {
		this.updateBaseSegment();

		this.updatePositions();

		this.updateMatrices();
	}

	private updateFlyingPositions() {
		const camZ = this.player.z - gameSettings.distanceCamToPlayer;
		const camX = this.player.x;
		const camY = this.player.y + gameSettings.cameraHeight;

		this.calculateDeltaX();

		this.position.x = Util.ease(this.position.x, camX, 0.2);
		this.position.y = Util.ease(this.position.y, camY, 0.2);
		this.position.z = camZ;

		this.targetPosition[0] = Util.ease(this.targetPosition[0], this.player.x, 0.2);
		this.targetPosition[1] = this.player.y;
		this.targetPosition[2] = this.player.z + gameSettings.camTargetZPlus;
	}

	private updatePositions() {
		if (this.player.status === FLYING) {
			return this.updateFlyingPositions();
		}

		const camZ = this.player.z - gameSettings.distanceCamToPlayer;

		const percent = Util.percentRemaining(camZ, gameSettings.segmentLength);
		const centerX = Util.interpolate(this.baseSegment.p1.world.x, this.baseSegment.p2.world.x, percent);
		const centerY = Util.interpolate(this.baseSegment.p1.world.y, this.baseSegment.p2.world.y, percent);
		const camY = Util.ease(this.position.y, centerY + gameSettings.cameraHeight, 0.2);

		this.calculateDeltaX();

		this.position.x = centerX + this.deltaX;
		this.position.y = camY;
		this.position.z = camZ;

		const targetZ = this.player.z + gameSettings.camTargetZPlus;
		const targetSegment = this.scene.road.findSegmentByZ(targetZ);
		const targetPercent = Util.percentRemaining(targetZ, gameSettings.segmentLength);
		const targetCenterX = Util.interpolate(targetSegment.p1.world.x, targetSegment.p2.world.x, targetPercent);
		const targetCenterY = Util.interpolate(targetSegment.p1.world.y, targetSegment.p2.world.y, targetPercent);

		this.targetPosition[0] = targetCenterX + this.targetDeltaX;
		this.targetPosition[1] = targetCenterY;
		this.targetPosition[2] = targetZ;
	}

	private updateBaseSegment() {
		const camZ = this.player.z - gameSettings.distanceCamToPlayer;
		this.baseSegment = this.scene.road.findSegmentByZ(camZ);
	}

	private updateMatrices() {
		const cameraPosition = [this.position.x, this.position.y, this.position.z];
		const up = [0, 1, 0];
		const target = this.targetPosition;

		// Compute the camera's matrix using look at.
		const cameraMatrix = m4.lookAt(cameraPosition, target, up);

		// Make a view matrix from the camera matrix.
		const viewMatrix = m4.inverse(cameraMatrix);

		this.viewProjectionMatrix = m4.multiply(this.projectionMatrix, viewMatrix);
	}

	private setMatrices() {
		const aspect = this.gameWidth / this.gameHeight;
		this.projectionMatrix = m4.perspective(this.fieldOfViewRadians, aspect, this.zNear, this.zFar);
	}

	private findCenterXByZ(z: number) {
		const segment = this.scene.road.findSegmentByZ(z);
		const percent = Util.percentRemaining(z, gameSettings.segmentLength);

		return Util.interpolate(segment.p1.world.x, segment.p2.world.x, percent);
	}

	private calculateDeltaX() {
		const centerX = this.findCenterXByZ(this.player.z);
		const deltaX = this.player.x - centerX;

		this.deltaX = Util.ease(this.deltaX, deltaX, 0.02);
		this.targetDeltaX = Util.ease(this.targetDeltaX, deltaX, 0.02);

		return deltaX;
	}
}
