import {Ball} from './Ball';

export class ArrowGuide extends Phaser.GameObjects.Image {
	private moveY: number;
	private moveUp: boolean;

	constructor(scene: any, x: number, y: number, texture: string, frame?: string) {
		super(scene, x, y, texture, frame);
		scene.add.existing(this);

		this.moveUp = true;
		this.moveY = 0;
	}

	public draw(target: Ball) {
		const ballDrawer = target.getDrawer();

		if (!ballDrawer) { return; }

		this.setPosition(ballDrawer.x, ballDrawer.y - ballDrawer.displayHeight - this.moveY);
		this.setScale(ballDrawer.scale);
		this.setDepth(ballDrawer.depth);
		this.setVisible(true);
		this.setActive(true);
	}

	public hide() {
		this.setVisible(false);
	}

	protected preUpdate(time: number, delta: number) {
		const dlt = delta * 0.01 * 3;

		if (this.moveUp) {
			this.moveY += dlt;
		} else {
			this.moveY -= dlt;
		}

		if (this.moveY >= 30) {
			this.moveUp = false;
		}

		if (this.moveY <= 0) {
			this.moveUp = true;
		}
	}
}
