import {gameSettings} from '../config/GameSettings';
import {Util} from './Util';
import {DOWN, FLYING, JUMP_DOWN, JUMP_UP, JUMPING, RUNNING, STANDING, UP} from '../constants/PlayerStatus';
import {TEXTURE} from '../constants/CommonType';
import {TAIL} from '../constants/SpriteName';
import {COLORS} from '../constants/BallColor';

export class BallTail {
	public scene: any;
	public target: any;
	public rope: any;
	public tweenScaleRope: any;
	public heightSegment: number;
	public tweenStartRope: any;
	public tweenStopRope: any;
	public parent: any;
	public ropePoints: any;
	private status: string;
	private tailLength: number;
	private tailHeight: number;
	private tailPoints3d: any[];
	private points: number;

	constructor(scene: any, parent: any) {
		this.scene = scene;
		this.target = parent.image;
		this.parent = parent;
		this.points = gameSettings.tailPoints;
		this.heightSegment = 1; // Dai = 15 segment

		this.rope = this.scene.add.rope(0, this.target.y, TEXTURE, TAIL, this.points, false);
		this.rope.setVisible(false);
		this.rope.setDepth(this.target.depth + 1);
		this.ropePoints = this.rope.points;
		this.status = null;
		this.tailPoints3d = [];

		this.tailLength = gameSettings.tailLength;
		this.initPoints();
	}

	public update(time: number, delta: number) {
		if (!this.status) { return; }

		this.updatePoints();

		this.updatePointsJump();

		this.updatePointsFly();

		this.projectPoints();

		this.updateColors();

		this.updateTail();

		this.rope.setDirty();
	}

	public reset() {
		this.rope.setVisible(false);
		this.status = null;
		this.initPoints();
	}

	public start() {
		this.rope.setVisible(true);
		this.status = 'STARTED';
	}

	public stop() {
		this.rope.setVisible(false);
		this.status = null;
	}

	public initPoints() {
		const targetZ = this.parent.trackPosition + gameSettings.distanceCamToPlayer;
		const pointDistance = gameSettings.tailLength / (this.points - 1);

		for (let i = 0; i < this.points; i++) {
			this.tailPoints3d.push({
				x: this.parent.x,
				y: this.parent.y,
				z: targetZ - pointDistance * (i - 1),
				deltaX: 0,
				deltaZ: 0,
				width: 100,
			});
		}
	}

	public updateColors() {
		const arr = [];
		const playerColor = this.scene.player.getFrame();
		// @ts-ignore
		const color = COLORS[playerColor];

		for (let i = 0; i < 12; i++) {
			arr.push(color);
		}
		this.rope.setColors(arr);
	}

	private updateTail() {
		this.rope.width = this.parent.image.displayWidth * 2 / 3;
	}

	private findCenterXByZ(z: number) {
		const segment = this.scene.road.findSegmentByZ(z);
		const percent = Util.percentRemaining(z, gameSettings.segmentLength);
		const centerX = Util.interpolate(segment.p1.world.x, segment.p2.world.x, percent);

		return centerX;
	}

	private calculateDeltaX() {
		const centerX = this.findCenterXByZ(this.parent.z);
		const deltaX = this.parent.x - centerX;

		return deltaX;
	}

	private updateFirstPoint3d() {
		this.tailPoints3d[0].x = this.parent.x;
		this.tailPoints3d[0].y = this.parent.y - gameSettings.playerWidth / 2;
		this.tailPoints3d[0].z = this.parent.z;
		this.tailPoints3d[0].deltaX = this.calculateDeltaX();
	}

	private updatePoints() {
		this.updateFirstPoint3d();

		const easeRate = 0.3;

		for (let i = 1; i < this.rope.points.length; i++) {
			const current = this.tailPoints3d[i];
			const before = this.tailPoints3d[i - 1];
			const pointDistance = current.deltaZ;

			const targetDeltaX = Util.ease(current.deltaX, before.deltaX, easeRate);

			const x = this.findCenterXByZ(before.z - pointDistance) + targetDeltaX;
			const z2 = pointDistance * pointDistance - (before.x - x) * (before.x - x);
			const b = z2 > 0 ? Math.sqrt(z2) : pointDistance / 2;
			const z = before.z - b;

			const segment = this.scene.road.findSegmentByZ(z);
			const percent = Util.percentRemaining(z, gameSettings.segmentLength);
			const y = Util.interpolate(segment.p1.world.y, segment.p2.world.y, percent);

			current.x = x;
			current.y = y;
			current.z = z;
			current.deltaX = targetDeltaX;
		}

		this.updatePointsDeltaZ();
	}

	private projectPoints() {
		const points = this.rope.points;

		for (let i = 0; i < points.length; i++) {
			const {
				x, y,
			} = this.scene.cameraManager.projectObject(this.tailPoints3d[i]);

			points[i].x = x;
			points[i].y = y;
		}
	}

	private updatePointsDeltaZ() {
		const pointDistance = gameSettings.tailLength / (this.points - 1);
		const easeRate = 0.05;

		for (let i = 1; i < this.rope.points.length; i++) {
			const current = this.tailPoints3d[i];

			const targetDeltaZ = Util.ease(current.deltaZ, pointDistance, easeRate);

			current.deltaZ = targetDeltaZ;
		}
	}

	private updatePointsJump() {
		const status = this.parent.getStatus();

		if (status !== JUMPING) { return; }

		this.jumping();
	}

	private updatePointsFly() {
		const status = this.parent.getStatus();

		if (status !== FLYING) { return; }

		this.flying();
	}

	private jumping() {
		switch (this.parent.jumpBehavior.getDirection()) {
			case UP:
				this.jumpUp();
				break;

			case DOWN:
				this.jumpDown();
				break;
		}
	}

	private jumpUp() {
		const points = this.rope.points;
		const jumpedHeight = this.parent.getHeight();

		for (let i = 1; i < points.length; i++) {
			const current = this.tailPoints3d[i];
			const before = this.tailPoints3d[i - 1];
			current.y =  Util.ease(current.y, before.y, (points.length - i) * 0.1);
			current.deltaZ = Util.ease(current.deltaZ, 0, 0.2);
		}
	}

	private jumpDown() {
		const points = this.rope.points;
		const jumpedHeight = this.parent.getHeight();

		for (let i = 1; i < points.length; i++) {
			const current = this.tailPoints3d[i];
			const before = this.tailPoints3d[i - 1];
			current.y =  Util.ease(current.y, before.y, 0.5);
		}
	}

	private flying() {
		switch (this.parent.flyBehavior.getDirection()) {
			case UP:
				this.flyUp();
				break;

			case DOWN:
				this.flyDown();
				break;
		}
	}

	private flyUp() {
		const points = this.rope.points;
		const maxHeight = this.parent.getMaxHeight();
		const flewHeight = this.parent.getHeight();
		const pointDistance = gameSettings.tailLength / (this.points - 1);

		if (!maxHeight) { return; }

		for (let i = 1; i < points.length; i++) {
			const current = this.tailPoints3d[i];
			const before = this.tailPoints3d[i - 1];
			const currentY = current.y;

			current.y =  before.y - (1 - flewHeight / maxHeight) * pointDistance;
			current.y = Phaser.Math.Clamp(current.y, currentY, before.y);

			current.deltaZ = Util.ease(current.deltaZ, 0, 0.2);
		}
	}

	private flyDown() {
		const points = this.rope.points;
		const maxHeight = this.parent.getMaxHeight();
		const flewHeight = this.parent.getHeight();
		const pointDistance = gameSettings.tailLength / (this.points - 1);

		for (let i = 1; i < points.length; i++) {
			const current = this.tailPoints3d[i];
			const before = this.tailPoints3d[i - 1];

			current.y =  before.y + (1 - flewHeight / maxHeight) * pointDistance;

			current.deltaZ = Util.ease(current.deltaZ, 0, 0.2);
		}
	}
}
