import { SegmentPoint } from './SegmentPoint';
import { gameSettings } from '../config/GameSettings';
import { DarkColors, LightColors } from './Colors';
import { Ledge } from './Ledge';
import { ExitDoor } from './ExitDoor';
import {Ball} from './Ball';
import {ROAD} from '../constants/SpriteName';

export class TrackSegment {
	public index: number;
	public p1: SegmentPoint;
	public p2: SegmentPoint;
	public looped: boolean = false;
	public fog: number = 0;
	public curve: number;
	public colors: any;
	public balls: Set<Ball>;
	public ledge: Ledge;
	public clip: number;
	public type: string;
	public alpha: number;
	public mesh: any;
	public visible: boolean;
	public isHighest: boolean;
	public exitDoor: ExitDoor;
	public rumbleLeft: any;
	public rumbleRight: any;
	public road: any;
	public upOrDownHill: boolean;
	public depth: number;
	public show: boolean;

	constructor(scene: any, z: number, curve: number, x1: number, x2: number, y: number, lastY: number, config: any) {
		const { type, alpha, visible, isHighest, upOrDownHill } = config;

		this.index = z;
		this.p1 = new SegmentPoint(x1, lastY, z * gameSettings.segmentLength);
		this.p2 = new SegmentPoint(x2, y, (z + 1) * gameSettings.segmentLength);
		this.colors = Math.floor(z / gameSettings.rumbleLength) % 2 ? DarkColors : LightColors;
		// this.colors = DarkColors;
		this.curve = curve;
		this.clip = 0;
		this.type = type;
		this.alpha = alpha || 0;
		this.visible = visible;
		this.show = true;
		this.isHighest = isHighest;
		this.upOrDownHill = upOrDownHill;
		this.depth = 0;

		this.balls = new Set<Ball>();
	}

	public reset() {
		this.balls.clear();
		this.ledge = null;
	}
}
