import { GameScene } from '../scenes/GameScene';
import {RESPAWNED, STANDING} from '../constants/PlayerStatus';

export class PointerController {
	private scene: GameScene;
	private mask: any;

	constructor(scene: GameScene) {
		this.scene = scene;
		this.init();
	}

	public init() {
		const gameWidth = this.scene.scale.gameSize.width;
		const gameHeight = this.scene.scale.gameSize.height;
		this.mask = this.scene.add.rectangle(gameWidth / 2, gameHeight / 2, gameWidth, gameHeight);
		this.mask.depth = 99999;
		this.mask.setInteractive();

		this.mask.on('pointerdown', () => {
			if (this.scene.player.status !== STANDING && this.scene.player.status !== RESPAWNED) {
				return;
			}

			if (this.scene.player.status === STANDING) {
				this.scene.player.run();
			}

			if (this.scene.player.status === RESPAWNED) {
				this.scene.player.runAfterRespawned();
			}

			this.scene.guild.hide();

			if (this.scene.musicManager.isPaused()) {
				this.scene.musicManager.resume();
			} else {
				this.scene.musicManager.stop();
				this.scene.musicManager.play();
			}

			this.hide();
		});
	}

	public hide() {
		this.mask.setVisible(false);
	}

	public show() {
		this.mask.setVisible(true);
	}
}

