import {GameScene} from '../scenes/GameScene';

export class Sky extends Phaser.GameObjects.Image {
	constructor(scene: GameScene, x: number, y: number, texture: string, frame?: string) {
		super(scene, x, y, texture, frame);

		scene.add.existing(this);
	}
	public update(time: number, delta: number) {

	}
}
