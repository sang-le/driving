import {Ball} from './Ball';
import {Util} from './Util';
import {Renderer} from './Renderer';
import {gameSettings} from '../config/GameSettings';
import {GameScene} from '../scenes/GameScene';

export class Wind extends Phaser.GameObjects.Sprite {
	public type: string;
	public position3D: Phaser.Math.Vector3;
	public percent: number;
	public wAngle: number;
	public wScale: number;
	public displayHeight: number;
	private gameWidth: number;
	private gameHeight: number;

	constructor(scene: GameScene, x: number, y: number, texture: string, frame?: string) {
		super(scene, x, y, texture, frame);
		// scene.add.existing(this);
		this.setActive(false);
		this.setVisible(false);
		this.position3D = new Phaser.Math.Vector3(0, 0, 0);
		this.percent = 0;
		this.wAngle = 0;
		this.wScale = 0;
		this.gameWidth = this.scene.scale.gameSize.width + 20;
		this.gameHeight = this.scene.scale.gameSize.height + 20;
	}

	public get x3d(): number { return this.position3D.x; }
	public set x3d(x: number) {
		this.position3D.x = x;
	}

	public get y3d(): number { return this.position3D.y; }
	public set y3d(y: number) {
		this.position3D.y = y;
	}

	public get z3d(): number { return this.position3D.z; }
	public set z3d(z: number) {
		this.position3D.z = z;
	}

	public draw(configs: any) {
		const { x, y, scale = 0, depth = 100000, angle, alpha, displayHeight } = configs;
		switch (this.type) {
			case 'TOP_LEFT':
				this.setScale(scale);
				this.setDepth(depth);
				this.setOrigin(0.5, 1);
				this.setPosition(x, y);
				this.setAngle(angle);
				this.displayHeight = this.displayHeight;
				this.alphaTopLeft = 0.5;
				this.alphaTopRight = 0.5;
				this.alphaBottomLeft = 0.01;
				this.alphaBottomRight = 0.01;
				break;

			case 'TOP_RIGHT':
				this.setScale(4 * scale, 8 * scale);
				this.setDepth(depth);
				this.setOrigin(0.5, 1);
				this.setPosition(x, y);
				this.setAngle(angle);
				// this.displayHeight = Phaser.Math.Between(300, 1000);
				this.alphaTopLeft = 0.5;
				this.alphaTopRight = 0.5;
				this.alphaBottomLeft = 0.01;
				this.alphaBottomRight = 0.01;
				break;

			case 'BOTTOM_LEFT':
				this.setScale(4 * scale, 8 * scale);
				this.setDepth(depth);
				this.setOrigin(0.5, 0);
				this.setPosition(x, y);
				this.setAngle(angle);
				// this.displayHeight = Phaser.Math.Between(300, 1000);
				this.alphaTopLeft = 0.5;
				this.alphaTopRight = 0.5;
				this.alphaBottomLeft = 0.01;
				this.alphaBottomRight = 0.01;
				break;

			case 'BOTTOM_RIGHT':
				this.setScale(4 * scale, 8 * scale);
				this.setDepth(depth);
				this.setOrigin(0.5, 0);
				this.setPosition(x, y);
				this.setAngle(angle);
				// this.displayHeight = Phaser.Math.Between(300, 1000);
				this.alphaTopLeft = 0.5;
				this.alphaTopRight = 0.5;
				this.alphaBottomLeft = 0.01;
				this.alphaBottomRight = 0.01;
				break;
		}
	}

	public hide() {
		this.setVisible(false);
		this.setActive(false);
	}

	public show() {
		this.setVisible(true);
		this.setActive(true);
	}

	protected preUpdate(time: number, delta: number) {
		const segment = this.scene.road.findSegmentByZ(this.z3d);
		if (!segment) { return; }

		const camY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, this.percent);

		const {
			x: x1, y: y1, w: w1, scale: scale1,
		} = Renderer.projectObject(segment.p1.world, segment.p1.world.y + this.y3d, segment.p2.world.x + Math.round(this.x3d), camY + gameSettings.cameraHeight,
			this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
			this.gameWidth, this.gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);

		const {
			x: x2, y: y2, w: w2, scale: scale2,
		} = Renderer.projectObject(segment.p2.world, segment.p2.world.y + this.y3d, segment.p2.world.x + Math.round(this.x3d), camY + gameSettings.cameraHeight,
			this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
			this.gameWidth, this.gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);

		const spriteY = Math.round(Util.interpolate(y1, y2, this.percent));
		const spriteX = Math.round(Util.interpolate(x1, x2, this.percent));
		const spriteScale = Util.interpolate(segment.p1.screen.scale, segment.p2.screen.scale, this.percent);
		this.show();

		this.draw({
			scale: this.wScale * gameSettings.spriteScale * spriteScale,
			x: spriteX,
			y: spriteY,
			depth: 10,
			angle: this.wAngle,
			alpha: 1,
		});
	}
}
