import {gameSettings} from '../config/GameSettings';

export class MusicManager {
	public scene: any;
	public music: any;

	constructor(scene: any) {
		this.scene = scene;

		this.music = this.scene.sound.add('music');
	}

	public play() {
		this.music.play();
	}

	public setRate() {
		this.music.setRate(this.scene.speedRate);
	}

	public reset() {
		this.stop();

		if (!this.scene.player.speed) { return; }

		this.play();
	}

	public stop() {
		this.music.stop();
	}

	public pause() {
		this.music.pause();
	}

	public resume() {
		this.music.resume();
	}

	public isPlaying() {
		return this.music.isPlaying;
	}

	public isPaused() {
		return this.music.isPaused;
	}
}
