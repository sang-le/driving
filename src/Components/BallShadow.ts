import {COLORS} from '../constants/BallColor';

export class BallShadow extends Phaser.GameObjects.Image {
	public color: string;

	constructor(scene: any, x: number, y: number, texture: string, frame?: string) {
		super(scene, x, y, texture, frame);
		this.setVisible(false);
		this.setActive(false);
	}

	public setColor(color: string) {
		if (!color || this.color === color) { return; }
		this.color = color;
		// @ts-ignore
		this.setTint(COLORS[color]);
	}

	public hide() {
		this.setActive(false);
		this.setVisible(false);
	}
}
