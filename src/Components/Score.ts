import {GameScene} from '../scenes/GameScene';
import {Util} from './Util';

export class Score {
	private scoreText: Phaser.GameObjects.BitmapText;
	private scoreNumber: number;
	private readonly scene: GameScene;

	constructor(scene: any, x: number, y: number) {
		this.scene = scene;
		this.scoreNumber = 0;
		this.scoreText = this.scene.add.bitmapText(x, y * Util.getDevicePixelRatio(), 'numbers', '0', 32 * Util.getDevicePixelRatio());
		this.scoreText.setOrigin(0.5, 0.5);
		this.scoreText.setAlpha(0.8);
	}

	public addScore(score: number) {
		this.scoreNumber += score;

		this.updateText();
	}

	public reset() {
		this.scoreNumber = 0;

		this.updateText();
	}

	private updateText() {
		this.scoreText.setText(this.scoreNumber.toString());
	}
}
