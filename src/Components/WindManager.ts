import {Util} from './Util';
import {gameSettings} from '../config/GameSettings';
import {Renderer} from './Renderer';
import {Wind} from './Wind';
import {Ball} from './Ball';
import {TEXTURE} from '../constants/CommonType';
import {BALL} from '../constants/SpriteName';

const TOP_LEFT_ANGLE = [
	{x: -1, y: 0},
	{x: -0.9, y: 0.1},
	{x: -0.8, y: 0.2},
	{x: -0.7, y: 0.3},
	{x: -0.6, y: 0.4},
	{x: -0.5, y: 0.5},
	{x: -0.4, y: 0.6},
	{x: -0.3, y: 0.7},
	{x: -0.2, y: 0.8},
	// {x: -0.1, y: 0.9},
	// {x: 0, y: 1},
];

export class WindManager {
	public scene: any;
	public winds: Set<Wind> = new Set<Wind>();
	public windObjectPools: Phaser.GameObjects.Group;

	private total: number;
	private topLeft: any[];
	private topRight: any[];
	private bottomLeft: any[];
	private bottomRight: any[];
	private segmentLength: number;
	private percent: number;
	private gameWidth: number;
	private gameHeight: number;

	constructor(scene: any, segmentLength: number, key: string) {
		this.scene = scene;

		this.segmentLength = segmentLength;
		this.percent = Util.percentRemaining(this.segmentLength, gameSettings.segmentLength);
		this.windObjectPools = new Phaser.GameObjects.Group(scene, null, {
			classType: Wind,
			defaultKey: key,
			maxSize: 30,
		});
		this.gameWidth = this.scene.scale.gameSize.width + 20;
		this.gameHeight = this.scene.scale.gameSize.height + 20;
		this.topLeft = [];
		this.topRight = [];
		this.bottomLeft = [];
		this.bottomRight = [];
		this.total = 0;

		// this.init();

		this.randomWind(5, this.segmentLength);
	}

	public init() {
		const displayOnAmountSegment = 5;
		const startSegment = this.segmentLength;
		const endSegment = this.segmentLength;

		const gameHeight = this.scene.scale.gameSize.height;
		const gameWidth = this.scene.scale.gameSize.width;
		const randomDataGenerator = new Phaser.Math.RandomDataGenerator();

		const center = {
			x: gameWidth / 2,
			y: gameHeight / 3,
		};

		const topLeft = [];
		const topRight = [];
		const bottomLeft = [];
		const bottomRight = [];

		// Random topLeft

		topLeft.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: Phaser.Math.Between(gameSettings.cameraHeight, gameSettings.cameraHeight * 2),
		});
		topLeft.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: Phaser.Math.Between(gameSettings.cameraHeight, gameSettings.cameraHeight * 2),
		});
		topLeft.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: Phaser.Math.Between(gameSettings.cameraHeight, gameSettings.cameraHeight * 2),
		});

		topLeft.forEach((item) => {
			const img = this.scene.add.image(center.x - 100 * Math.abs(item.x), center.y - 100 * Math.abs(item.y), 'wind');
			img.positionName = 'TOP_LEFT';
			img.pX = item.x;
			img.pY = item.y;
			img.setOrigin(0.5, 0);
			img.setScale(0.2);
			img.setAngle(10);
			img.setDepth(10);
			img.setAngle(180 + Math.atan(item.x / item.y) * 180 / Math.PI);
			img.displayHeight = Phaser.Math.Between(300, 1000);
			img.alphaTopLeft = 0.5;
			img.alphaTopRight = 0.5;
			img.alphaBottomLeft = 0.1;
			img.alphaBottomRight = 0.1;
			img.initScale = randomDataGenerator.realInRange(0.001, 0.05);

			this.sprites.add(img);
		});

		// Random topRight

		topRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(0, 1),
		});
		topRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(0, 1),
		});
		topRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(0, 1),
		});
		topRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(0, 1),
		});
		topRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(0, 1),
		});

		topRight.forEach((item) => {
			const img = this.scene.add.image(center.x + 100 * Math.abs(item.x), center.y - 100 * Math.abs(item.y), 'wind');
			img.positionName = 'TOP_RIGHT';
			img.pX = item.x;
			img.pY = item.y;
			img.setOrigin(0.5, 0);
			img.setScale(0.2);
			img.setDepth(10);
			img.setAngle(180 + Math.atan(item.x / item.y) * 180 / Math.PI);
			img.displayHeight = Phaser.Math.Between(300, 1000);
			img.alphaTopLeft = 0.5;
			img.alphaTopRight = 0.5;
			img.alphaBottomLeft = 0.1;
			img.alphaBottomRight = 0.1;
			img.initScale = randomDataGenerator.realInRange(0.001, 0.05);

			this.sprites.add(img);
		});

		// Random bottomRight

		bottomRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomRight.push({
			x: randomDataGenerator.realInRange(0, 1),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomRight.forEach((item) => {
			const img = this.scene.add.image(center.x + 100 * Math.abs(item.x), center.y + 100 * Math.abs(item.y), 'wind');
			img.positionName = 'BOTTOM_RIGHT';
			img.pX = item.x;
			img.pY = item.y;
			img.setOrigin(0.5, 0);
			img.setScale(0.2);
			img.setDepth(10);
			img.setAngle(Math.atan(item.x / item.y) * 180 / Math.PI);
			img.displayHeight = Phaser.Math.Between(300, 1000);
			img.alphaTopLeft = 0.5;
			img.alphaTopRight = 0.5;
			img.alphaBottomLeft = 0.1;
			img.alphaBottomRight = 0.1;
			img.initScale = randomDataGenerator.realInRange(0.001, 0.05);

			this.sprites.add(img);
		});

		// Random bottomLeft

		bottomLeft.push({
			x: randomDataGenerator.realInRange(-1, 0),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomLeft.push({
			x: randomDataGenerator.realInRange(-1, 0),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomLeft.push({
			x: randomDataGenerator.realInRange(-1, 0),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomLeft.push({
			x: randomDataGenerator.realInRange(-1, 0),
			y: randomDataGenerator.realInRange(-1, 0),
		});

		bottomLeft.forEach((item) => {
			const img = this.scene.add.image(center.x - 100 * Math.abs(item.x), center.y + 100 * Math.abs(item.y), 'wind');
			img.positionName = 'BOTTOM_LEFT';
			img.pX = item.x;
			img.pY = item.y;
			img.setOrigin(0.5, 0);
			img.setScale(0.2);
			img.setDepth(10);
			img.setAngle(Math.atan(item.x / item.y) * 180 / Math.PI);
			img.displayHeight = Phaser.Math.Between(300, 1000);
			img.alphaTopLeft = 0.5;
			img.alphaTopRight = 0.5;
			img.alphaBottomLeft = 0.1;
			img.alphaBottomRight = 0.1;
			img.initScale = randomDataGenerator.realInRange(0.001, 0.05);

			this.sprites.add(img);
		});

		// Origin Y = 1 -> Tu duoi len
		// Origin Y = 0 -> Tu tren xuong

		console.log(this.sprites);
	}

	public randomWind(amount: number = 0, segmentLength: number) {
		const randomDataGenerator = new Phaser.Math.RandomDataGenerator();
		const maxX3d = Util.tan(gameSettings.fieldOfView / 2) * segmentLength;
		let i = 0;
		let zz = 0;

		// const tlX = randomDataGenerator.realInRange(-0.3, 0);
		// const tlY = randomDataGenerator.realInRange(0, 0.1);
		//
		// this.topLeft.push({
		// 	type: 'TOP_LEFT',
		// 	x: 0 - tlX * maxX3d / 2,
		// 	y: (1 + tlY) * gameSettings.cameraHeight,
		// 	z: segmentLength + i * gameSettings.segmentLength,
		// 	angle: Math.atan(tlX / tlY) * 180 / Math.PI,
		// 	scale: randomDataGenerator.realInRange(0.3, 0.5),
		// });
		//
		// return;

		console.log(Math.atan(1 / 1) * 180 / Math.PI);

		while (this.topLeft.length < amount) {
			let scale = 0;
			let pStartX = 0;
			let pStartY = 0;
			let pEndX = 0;
			let pEndY = 0;
			let displayHeight = 0;

			if (i < 3) {
				scale = 0.5;
				pStartX = 0.1;
				pStartY = 0.1;
				pEndX = 0.3;
				pEndY = 0.3;
				displayHeight = Phaser.Math.Between(200, 400);
			} else if (i < 6) {
				scale = 1;
				pStartX = 0.4;
				pStartY = 1;
				pEndX = 0.6;
				pEndY = 2;
				displayHeight = Phaser.Math.Between(400, 600);
			} else {
				scale = 1.7;
				pStartX = 0.6;
				pStartY = 3;
				pEndX = 0.8;
				pEndY = 4;
				displayHeight = Phaser.Math.Between(600, 1000);
			}

			const tlX = randomDataGenerator.realInRange(-pEndX, pStartX);
			const tlY = randomDataGenerator.realInRange(pStartY, pEndY);

			const tl = randomDataGenerator.pick(TOP_LEFT_ANGLE);
			this.topLeft.push({
				type: 'TOP_LEFT',
				x: 0 - tl.x * maxX3d / 2,
				y: (1 + tl.y) * gameSettings.cameraHeight,
				z: segmentLength ,
				angle: Math.atan(tl.x / tl.y) * 180 / Math.PI,
				scale,
				displayHeight: Phaser.Math.Between(300, 1000),
			});

			const trX = randomDataGenerator.realInRange(pStartX, pEndX);
			const trY = randomDataGenerator.realInRange(pStartY, pEndY);
			this.topRight.push({
				type: 'TOP_RIGHT',
				x: 0 - trX * maxX3d / 2,
				y: (1 + trY) * gameSettings.cameraHeight,
				z: segmentLength + i * gameSettings.segmentLength,
				angle: Math.atan(trX / trY) * 180 / Math.PI,
				scale,
			});

			const blX = randomDataGenerator.realInRange(-pEndX, pStartX);
			const blY = randomDataGenerator.realInRange(-pEndY, pStartY);
			this.bottomLeft.push({
				type: 'BOTTOM_LEFT',
				x: 0 - blX * maxX3d / 2,
				y: 0, // (1 + blY) * gameSettings.cameraHeight,
				z: segmentLength + i * gameSettings.segmentLength,
				angle: Math.atan(blX / blY) * 180 / Math.PI,
				scale,
			});

			const brX = randomDataGenerator.realInRange(pStartX, pEndX);
			const brY = randomDataGenerator.realInRange(-pEndY, pStartY);
			this.bottomRight.push({
				type: 'BOTTOM_RIGHT',
				x: 0 - brX * maxX3d / 2,
				y: 0, // (1 + brY) * gameSettings.cameraHeight,
				z: segmentLength + i * gameSettings.segmentLength,
				angle: Math.atan(brX / brY) * 180 / Math.PI,
				scale,
			});

			i++;
			zz += 5;
		}

		this.total = amount * 4;
	}

	public hideAll() {
		// this.sprites.forEach((item) => {
		// 	item.setVisible(false);
		// });
	}

	public draw(item: any) {
		const wind = this.windObjectPools.get();

		if (!wind) { return; }

		wind.x3d = item.x;
		wind.y3d = item.y;
		wind.z3d = item.z;
		wind.wAngle = item.angle;
		wind.wScale = item.scale;
		wind.type = item.type;
		wind.displayHeight = item.displayHeight;

		this.winds.add(wind);
	}

	public createWind() {
		const wind = this.windObjectPools.get();

		if (!wind) { return; }

		const randomDataGenerator = new Phaser.Math.RandomDataGenerator();
		const tl = randomDataGenerator.pick(TOP_LEFT_ANGLE);

		wind.x3d = tl.x;
		wind.y3d = tl.y;
		// wind.z3d = item.z;
		// wind.wAngle = item.angle;
		// wind.wScale = item.scale;
		// wind.type = item.type;
		// wind.displayHeight = item.displayHeight;

		this.winds.add(wind);
	}

	public update(time: number, delta: number) {
		this.killWind();

		if (!this.segmentLength || this.scene.player.trackPosition > this.segmentLength) {
			// this.scene.scene.pause();
			return;
		}

		// this.draw(this.topLeft[0]);
		//
		// return;

		if (this.windObjectPools.getTotalUsed() >= this.total) {
			return;
		}

		this.topLeft.forEach((item: any) => {
			this.draw(item);
		});

		// this.topRight.forEach((item: any) => {
		// 	this.draw(item);
		// });
		//
		// this.bottomLeft.forEach((item: any) => {
		// 	this.draw(item);
		// });
		//
		// this.bottomRight.forEach((item: any) => {
		// 	this.draw(item);
		// });

		// const gameWidth = this.scene.scale.gameSize.width + 20;
		// const gameHeight = this.scene.scale.gameSize.height + 20;
		// const spriteScale = Util.interpolate(segment.p1.screen.scale, segment.p2.screen.scale, this.percent);
		// const camY = Util.interpolate(segment.p1.world.y, segment.p2.world.y, this.percent);
		//
		// const {
		// 	x: x1, y: y1, w: w1, scale: scale1,
		// } = Renderer.projectObject(segment.p1.world, segment.p1.world.y + gameSettings.cameraHeight + 10, 0, camY + gameSettings.cameraHeight,
		// 	this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
		// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);
		//
		// const {
		// 	x: x2, y: y2, w: w2, scale: scale2,
		// } = Renderer.projectObject(segment.p2.world, segment.p2.world.y + gameSettings.cameraHeight + 10, 0, camY + gameSettings.cameraHeight,
		// 	this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
		// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);
		//
		// const spriteY = Math.round(Util.interpolate(y1, y2, this.percent));

		// console.log(spriteY)

		// console.log({
		// 	y1,
		// 	y2,
		// 	x1,
		// 	x2
		// })

		// this.sprites.forEach((item) => {
		// 	// const {
		// 	// 	x: x1, y: y1, w: w1, scale: scale1,
		// 	// } = Renderer.projectObject({x: 0, y: 0, z: 60000}, segment.p1.world.y + 100000, 0, camY + gameSettings.cameraHeight,
		// 	// 	this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
		// 	// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);
		// 	//
		// 	// const {
		// 	// 	x: x2, y: y2, w: w2, scale: scale2,
		// 	// } = Renderer.projectObject({x: 0, y: 0, z: 63000}, segment.p2.world.y + 100000, 0, camY + gameSettings.cameraHeight,
		// 	// 	this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
		// 	// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);
		// 	//
		// 	//
		// 	// const spriteY = Math.round(Util.interpolate(y1, y2, this.percent));
		// 	// const spriteX = Math.round(Util.interpolate(x1, x2, this.percent));
		//
		// 	// switch (item.positionName) {
		// 	// 	case 'TOP_LEFT':
		// 	// 		item.setPosition(spriteX - 200 * Math.abs(item.pX), spriteY - 200 * Math.abs(item.pY));
		// 	// 		break;
		// 	//
		// 	// 	case 'TOP_RIGHT':
		// 	// 		item.setPosition(spriteX + 200 * Math.abs(item.pX), spriteY - 200 * Math.abs(item.pY));
		// 	// 		break;
		// 	//
		// 	// 	case 'BOTTOM_LEFT':
		// 	// 		item.setPosition(spriteX - 200 * Math.abs(item.pX), spriteY + 200 * Math.abs(item.pY));
		// 	// 		break;
		// 	//
		// 	// 	case 'BOTTOM_RIGHT':
		// 	// 		item.setPosition(spriteX + 200 * Math.abs(item.pX), spriteY + 200 * Math.abs(item.pY));
		// 	// 		break;
		// 	// }
		//
		// 	item.setScale(item.initScale * gameSettings.spriteScale * spriteScale);
		// 	// item.setScale(gameSettings.spriteScale * spriteScale);
		//
		// 	if (!item.visible) {
		// 		item.setVisible(true);
		// 	}
		// });
	}

	private killWind() {
		this.winds.forEach((wind) => {
			if (wind.z3d < this.scene.player.trackPosition) {
				this.winds.delete(wind);
				this.windObjectPools.killAndHide(wind);
			}
		});
	}
}
