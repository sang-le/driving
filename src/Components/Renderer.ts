import { SegmentPoint } from './SegmentPoint';
import { Util } from './Util';
import { gameSettings } from '../config/GameSettings';
import { GameScene } from '../scenes/GameScene';
import {FLYING} from '../constants/PlayerStatus';
import {scaleCanvasWebGL} from '../utils/scaleCanvas';

export class Renderer {
	public static project(sp: SegmentPoint, cameraX: number, cameraY: number, cameraZ: number, cameraDepth: number, width: number, height: number, roadWidth: number) {
		const x3d = (sp.world.x || 0) - cameraX;
		const y3d = (sp.world.y || 0) - cameraY;
		const z3d = (sp.world.z || 0) - cameraZ;
		// sp.camera.x = (sp.world.x || 0) - cameraX;
		// sp.camera.y = (sp.world.y || 0) - cameraY;
		// sp.camera.z = (sp.world.z || 0) - cameraZ;
		sp.screen.scale = cameraDepth / z3d;
		sp.screen.x = Math.round((width / 2) + (sp.screen.scale * x3d * width / 2));
		sp.screen.y = Math.round((height / 3) - (sp.screen.scale * y3d * height / 3));
		sp.screen.w = Math.round((sp.screen.scale * roadWidth * width / 2));
	}

	public static projectObject(segment: any, y3d: any, cameraX: number, cameraY: number, cameraZ: number, cameraDepth: number, width: number, height: number, roadWidth: number) {
		const translateX = (segment.x || 0) - cameraX;
		const translateY = (y3d || 0) - cameraY;
		const translateZ = (segment.z || 0) - cameraZ;

		const scale = cameraDepth / translateZ;
		const x = Math.round((width / 2) + (scale * translateX * width / 2));
		const y = Math.round((height / 3) - (scale * translateY * height / 3));
		const w = Math.round((scale * roadWidth * width / 2));

		return {
			scale,
			x,
			y,
			w,
		};
	}

	public static drawSegment(ctx: Phaser.GameObjects.Graphics, width: number, lanes: number, x1: number, y1: number, w1: number, x2: number, y2: number, w2: number, colors: any) {
		const r1 = Util.rumbleWidth(w1, lanes);
		const r2 = Util.rumbleWidth(w2, lanes);
		const l1 = Util.laneMarkerWidth(w1, lanes);
		const l2 = Util.laneMarkerWidth(w1, lanes);
		const h = y1 - y2;

		// ctx.fillStyle(colors.GRASS);
		// ctx.fillRect(-10, y2, width, h);

		Renderer.drawPolygon(ctx, x1 - w1 - r1, y1, x1 - w1, y1, x2 - w2, y2, x2 - w2 - r2, y2, colors.RUMBLE);
		Renderer.drawPolygon(ctx, x1 + w1 + r1, y1, x1 + w1, y1, x2 + w2, y2, x2 + w2 + r2, y2, colors.RUMBLE);
		Renderer.drawPolygon(ctx, x1 - w1, y1, x1 + w1, y1, x2 + w2, y2, x2 - w2, y2, colors.ROAD);

		const laneW1 = w1 * 2 / lanes;
		const laneW2 = w2 * 2 / lanes;
		const laneX1 = x1 - w1 + laneW1 + laneW1 / 2;
		const laneX2 = x2 - w2 + laneW2 + laneW2 / 2;

		if (colors.LANE) {
			// for (let lane = 1; lane < lanes; laneX1 += laneW1, laneX2 += laneW2, lane++) {
			// 	Renderer.drawPolygon(ctx, laneX1 - l1 / 2, y1, laneX1 + l1 / 2, y1, laneX2 + l2 / 2, y2, laneX2 - l2 / 2, y2, colors.LANE);
			// }

			Renderer.drawPolygon(ctx, laneX1 - l1 / 2, y1, laneX1 + l1 / 2, y1, laneX2 + l2 / 2, y2, laneX2 - l2 / 2, y2, colors.LANE);
		}
	}

	// -------------

	public scene: GameScene;
	public roadGraphics: Phaser.GameObjects.Graphics;

	constructor(scene: GameScene, depth: number = 0) {
		this.scene = scene;
		this.roadGraphics = this.scene.add.graphics().setDepth(depth);
	}

	public drawRoadPolygon(segment: any, width: number, lanes: number, x1: number, y1: number, w1: number, x2: number, y2: number, w2: number) {
		const topLeft = { x: x1 - w1, y: y1 };
		const topRight = { x: x1 + w1, y: y1 };
		const bottomLeft = { x: x2 - w2, y: y2 };
		const bottomRight = { x: x2 + w2, y: y2 };
		const topMid = { x: x1, y: y1 };
		const topCenter = {
			x: x1, y: y1,
		};
		const bottomCenter = { x: x2 , y: y2 };

		const line1 = {
			p1: topLeft, p2: bottomRight,
		};

		const line2 = {
			p1: topRight, p2: bottomLeft,
		};

		const center = this.findIntersectionPoint(line1, line2);

		const vertices = [
			// topLeft.x, topLeft.y,
			// bottomLeft.x, bottomLeft.y,
			// bottomRight.x, bottomRight.y,
			//
			// topLeft.x, topLeft.y,
			// bottomRight.x, bottomRight.y,
			// topRight.x, topRight.y,

			topLeft.x, topLeft.y,
			topRight.x, topRight.y,
			center.x, center.y,

			topRight.x, topRight.y,
			bottomRight.x, bottomRight.y,
			center.x, center.y,

			bottomRight.x, bottomRight.y,
			bottomLeft.x, bottomLeft.y,
			center.x, center.y,

			bottomLeft.x, bottomLeft.y,
			topLeft.x, topLeft.y,
			center.x, center.y,
		];
		segment.road.vertices = vertices;

		// const r1 = Util.rumbleWidth(w1, lanes);
		// const r2 = Util.rumbleWidth(w2, lanes);

		// this.drawRumble(segment.rumbleLeft, x1 - w1 - r1, y1, x1 - w1, y1, x2 - w2 - r2, y2, x2 - w2, y2);
		// this.drawRumble(segment.rumbleRight, x1 + w1, y1, x1 + w1 + r1, y1, x2 + w2, y2, x2 + w2 + r2, y2);
	}

	public drawRumble(rumble: any, x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, x4: number, y4: number) {
		const topLeft = { x: x1, y: y1 };
		const topRight = { x: x2, y: y2 };
		const bottomLeft = { x: x3, y: y3 };
		const bottomRight = { x: x4, y: y4 };

		const vertices = [
			topLeft.x, topLeft.y,
			bottomLeft.x, bottomLeft.y,
			bottomRight.x, bottomRight.y,

			topLeft.x, topLeft.y,
			bottomRight.x, bottomRight.y,
			topRight.x, topRight.y,
		];

		rumble.vertices = vertices;
	}

	public update(time: number, delta: number): void {
		this.roadGraphics.clear();
		this.drawRoad();
	}

	public drawRoad(): void {
		const gameWidth = this.scene.scale.gameSize.width + 20;
		const gameHeight = this.scene.scale.gameSize.height + 20;
		const segmentLength = this.scene.road.segments.length;

		const baseSegment = this.scene.road.findSegmentByZ(this.scene.player.trackPosition);
		// const camY = this.scene.player.y;

		const playerSegment = this.scene.road.findSegmentByZ(this.scene.player.trackPosition + gameSettings.distanceCamToPlayer);
		const playerPercent = Util.percentRemaining(this.scene.player.trackPosition + gameSettings.distanceCamToPlayer, gameSettings.segmentLength);
		let camY = Util.interpolate(playerSegment.p1.world.y, playerSegment.p2.world.y, playerPercent);

		switch (this.scene.player.status) {
			case FLYING:
				camY = this.scene.player.y;
				break;
		}

		const basePercent = Util.percentRemaining(this.scene.player.trackPosition, gameSettings.segmentLength);

		let maxY = gameHeight; // used for clipping things behind a hill
		let roadCenterX = 0;
		let deltaX = -(baseSegment.curve * basePercent);

		// draw road front to back
		for (let n = 0; n < gameSettings.drawDistance; n++) {
			const segmentIndex = (baseSegment.index + n) % this.scene.road.segments.length;
			const segment = this.scene.road.segments[segmentIndex];

			if (segment.visible) {
				segment.clip = maxY;
				segment.looped = segment.index < baseSegment.index;


				this.scene.cameraManager.projectSegment(segment.p1);

				this.scene.cameraManager.projectSegment(segment.p2);

				// Renderer.project(segment.p1, 0 * gameSettings.roadWidth - roadCenterX, camY + gameSettings.cameraHeight,
				// 	this.scene.player.trackPosition - (segment.looped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
				// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);
				//
				// Renderer.project(segment.p2, 0 * gameSettings.roadWidth - roadCenterX - deltaX, camY + gameSettings.cameraHeight,
				// 	this.scene.player.trackPosition - (segment.lo
				// 				const {p1, p2} = segment;
				//
				// 				p1.x += roadCenterX;
				// 				p2.x += roadCenterX + deltaX;oped ? this.scene.road.trackLength : 0), gameSettings.cameraDepth,
				// 	gameWidth, gameHeight - gameSettings.projectYCompensation, gameSettings.roadWidth);

				roadCenterX = roadCenterX + deltaX;
				deltaX = deltaX + segment.curve;

				// if (segment.p1.camera.z <= gameSettings.cameraDepth || segment.p2.screen.y >= maxY || segment.p2.screen.y >= segment.p1.screen.y) {
				// if (segment.p1.camera.z <= gameSettings.cameraDepth || segment.p2.screen.y > gameHeight) {
				if ((segment.p1.camera.z && segment.p1.camera.z <= gameSettings.cameraDepth) || segment.p2.screen.y > gameHeight) {
					segment.road.setVisible(false);

					continue;
				}

				// segment.road.setVisible(true);
				segment.depth = segmentLength - segment.index;
				segment.road.setDepth(2);

				if (segment.p2.screen.y >= maxY || segment.p2.screen.y >= segment.p1.screen.y) {
					segment.road.setDepth(1);
				}

				if (!(segment.p2.screen.y >= maxY || segment.p2.screen.y >= segment.p1.screen.y)) {
					maxY = segment.p2.screen.y;
				}

				this.drawRoadPolygon(segment, gameWidth, gameSettings.lanes,
					segment.p1.screen.x - 10, segment.p1.screen.y, segment.p1.screen.w,
					segment.p2.screen.x - 10, segment.p2.screen.y, segment.p2.screen.w);
			}
		}
	}

	private findIntersectionPoint(line1: any, line2: any) {

		return {x: (line1.p1.x + line2.p1.x) / 2, y: (line1.p1.y + line1.p2.y) / 2};

		const a1 = (line1.p2.y - line1.p1.y) / (line1.p2.x - line1.p1.x);
		const b1 = line1.p2.y - a1 * line1.p2.x;

		const a2 = (line2.p2.y - line2.p1.y) / (line2.p2.x - line2.p1.x);
		const b2 = line2.p2.y - a2 * line2.p2.x;

		// find intersection point
		// y = a1x + b1
		// y = a2x + b2
		// a1x + b1 = a2x + b2
		// (a1 - a2)x = b2 - b1
		const x = (b2 - b1) / (a1 - a2);
		const y = a1 * x + b1;

		return {x, y: y + 50};
	}
}
